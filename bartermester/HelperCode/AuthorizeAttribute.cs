﻿using BarterMester.Models.DemandManagement;
using BarterMester.Models.UserManagement;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static BarterMester.Services.UserService;

namespace BarterMester.HelperCode
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        private TokenType _tokenType;

        public AuthorizeAttribute(TokenType tokenType) 
        {
            _tokenType = tokenType;
        }

        //public void OnAuthorization(AuthorizationFilterContext context)
        //{
        //    var user = (User)context.HttpContext.Items["User"];
        //    if (_tokenType.ToString() != "InvitationToken")
        //    {
        //         //HANDLE SPECIFIC HTTPSTATUS FOR missing user value
        //        if (user != null)
        //        {
        //            if(_tokenType.ToString() != context.HttpContext.Items["TokenType"].ToString())
        //            {
        //                context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
        //            }
        //            context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
        //        }
        //    }
        //}

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = (User)context.HttpContext.Items["User"];

            if(user == null)
            {
                context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
            }
            else
            {
                if (_tokenType.ToString() != context.HttpContext.Items["TokenType"].ToString())
                {
                    context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
                }
            }
            
        }
    }
}
