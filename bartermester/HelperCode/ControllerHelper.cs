﻿using BarterMester.Models;
using BarterMester.Models.DBContext;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static BarterMester.Services.UserService;

namespace BarterMester.HelperCode
{
    public class ControllerHelper
    {
        private readonly BarterMesterDBContext _context;
        private readonly HttpContext _httpContext;
        public ControllerHelper(BarterMesterDBContext context)
        {
            _context = context;
        }
        public Guid GetUserId()
        {
            var userId = (Guid)_httpContext.Items["UserId"];
            return userId;
        }

        public Guid GetStatusGuidByInt(int intId)
        {
            return _context.StatusModels.Where(a => a.Id == intId).FirstOrDefault().Guid;
        }
    }
}
