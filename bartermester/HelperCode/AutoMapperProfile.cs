﻿using AutoMapper;
using BarterMester.Models.DemandManagement;
using BarterMester.Models.UserManagement;

namespace BarterMester.HelperCode
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserModel>();
            CreateMap<RegisterUserRequest, User>();
            CreateMap<UpdateModel, User>();
        }
    }
}
