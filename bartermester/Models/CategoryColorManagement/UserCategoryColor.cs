﻿using BarterMester.Models.ItemManagement;
using BarterMester.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace bartermester.Models.CategoryColorManagement
{
    public class UserCategoryColor
    {
           [Key]
            public Guid Id { get; set; }
            [StringLength(255)]
            public string color { get; set; }
         
            [ForeignKey("CategoryModel")]
            public Guid categoryId { get; set; }
            public CategoryModel CategoryModel { get; set; }
           
            //User here = Requestor / Áruigénylő
            [ForeignKey("User")]
            public Guid UserId { get; set; }
            public User User { get; set; }
            //public DateTime PostedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
         
        }
        public class UserCategoryColorDto
        {

            public string color { get; set; }
            public string categoryId { get; set; }
            public string UserId { get; set; }

        }




 }

