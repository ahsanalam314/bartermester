﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace bartermester.Models.FooterManagement
{
    
    public class FooterModel
    {
        [Key]
        public Guid Id { get; set; }
        [StringLength(255)]
        public string text { get; set; }

        public DateTime UpdatedAt { get; set; }


    }
    public class FooterDto
    {
        //public int id { get; set; }
        public string text { get; set; }
        
    }

}
