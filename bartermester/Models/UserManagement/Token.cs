﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BarterMester.Models.UserManagement
{
    public class Token
    {
        [Key]
        public Guid Id { get; set; }
        [StringLength(255)]
        public string TokenDetail { get; set; }
        [StringLength(255)]
        public string InvitorId { get; set; }
        public bool IsUsed { get; set; }
        [StringLength(255)]
        public string Email { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UsedAt { get; set; }
    }
}
