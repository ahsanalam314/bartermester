﻿using System.ComponentModel.DataAnnotations;

namespace BarterMester.Models.UserManagement
{
    public class RegisterUserRequest
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public bool HasAcceptedGDPR { get; set; }

        [Required]
        public bool HasAcceptedTermsAndConditions { get; set; }
    }
}
