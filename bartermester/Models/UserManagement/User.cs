﻿using System;
using System.Collections.Generic;
using BarterMester.Models.BarterManagement;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;

namespace BarterMester.Models.UserManagement
{
    public class User
    {
        public Guid Id { get; set; }
        [StringLength(255)]
        public string FirstName { get; set; }
        [StringLength(255)]
        public string LastName { get; set; }
        [StringLength(255)]
        public string Email { get; set; }
        [StringLength(255)]
        public string Username { get; set; }
        [JsonIgnore]
        public byte[] PasswordHash { get; set; }
        [JsonIgnore]
        public byte[] PasswordSalt { get; set; }
        public DateTime RegisteredAt { get; set; }
        [StringLength(255)]
        public string Role { get; set; }
        [ForeignKey("Invitor")]
        public Guid InvitorId { get; set; }
        [JsonIgnore]
        public User Invitor { get; set; }
        public bool HasAcceptedGDPR { get; set; }
        public bool HasAcceptedTermsAndConditions { get; set; }

        [JsonIgnore]
        public ICollection<Barter> Barters { get; set; }
    }
}