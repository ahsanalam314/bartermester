﻿namespace BarterMester.Models.UserManagement
{
    public class PasswordChangeRequest
    {
        public string NewPassword { get; set; }
    }
}
