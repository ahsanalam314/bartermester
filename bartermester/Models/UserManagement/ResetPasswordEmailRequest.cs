﻿namespace BarterMester.Models.UserManagement
{
    public class ResetPasswordEmailRequest
    {
        public string Email { get; set; }
    }
}
