﻿using BarterMester.Models.ItemManagement;
using BarterMester.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace BarterMester.Models.DemandManagement
{
    public class Offer
    {
        [Key]
        public Guid Id { get; set; }
        [StringLength(255)]
        public string Name { get; set; }
        [Column(TypeName = "decimal(18,3)")]
        public decimal Amount { get; set; }
        [StringLength(255)]
        public string TransportModel { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        [ForeignKey("LocationModel")]
        public Guid? LocationModelId { get; set; }
        public LocationModel LocationModel { get; set; }
        [ForeignKey("OfferStatus")]
        public Guid OfferStatusGuid { get; set; }
        public OfferStatus OfferStatus { get; set; }
        [ForeignKey("Demand")]
        public Guid DemandId { get; set; }
        public Demand Demand { get; set; }
        //User here = Bidder / Ajánlatadó
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public User User{ get; set; }
        public DateTime PostedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        [JsonIgnore]
        public ICollection<OfferImage> OfferImages { get; set; }
    }
    public class OfferDto
    {
        public string Name { get; set; }
        public string TransportModel { get; set; }
        [Column(TypeName = "decimal(18,3)")]
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public string LocationModelId { get; set; }
        public string DemandId { get; set; }
    }
    public class OfferImage
    {
        [Key]
        public Guid Id { get; set; }
        [StringLength(255)]
        public string Name { get; set; }
        public DateTime PostedAt { get; set; }
        [ForeignKey("Offer")]
        public Guid OfferId { get; set; }
        public Offer Offer { get; set; }
        [StringLength(255)]
        public string ImagePath { get; set; }
    }
    public class OfferImageDto
    {
        public string Name { get; set; }
    }
    public class OfferStatus
    {
        public enum statusList { }

        [Key]
        public Guid Guid { get; set; }
        public int Id { get; set; }
        [StringLength(255)]
        public string Name { get; set; }

        [JsonIgnore]
        public ICollection<Offer> Offers { get; set; }
    }
}
