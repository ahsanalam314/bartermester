﻿using BarterMester.Models.ItemManagement;
using BarterMester.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BarterMester.Models.DemandManagement
{
    public class Demand
    {
        [Key]
        public Guid Id { get; set; }
        [StringLength(255)]
        public string Name { get; set; }
        public string Description { get; set; }
        [ForeignKey("CategoryModel")]
        public Guid CategoryModelId { get; set; }
        public CategoryModel CategoryModel { get; set; }
        [ForeignKey("StatusModel")]
        public Guid StatusModelGuid { get; set; }
        public StatusModel StatusModel { get; set; }
        //User here = Requestor / Áruigénylő
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public User User { get; set; }
        public DateTime PostedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        [JsonIgnore]
        public ICollection<Offer> Offers { get; set; }
        //NEM JAVASLOM, BELESZÓL AZ AJÁNLATADÓ ÁRAZÁSÁBA, HA LÁTJA MENNYIT AKAR / HAJLANDÓ KIADNI A KERESŐ
        //public decimal MaxPrice { get; set; }
    }
    public class DemandDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string CategoryModelId { get; set; }
        public string StatusModelGuid { get; set; }
        public string UserId { get; set; }
        //NEM JAVASLOM, BELESZÓL AZ AJNÁNLATADÓ ÁRAZÁSÁBA, HA LÁTJA MENNYIT AKAR / HAJLANDÓ KIADNI A KERESŐ
        //public decimal MaxPrice { get; set; }
    }
}
