﻿using BarterMester.Models.DemandManagement;
using BarterMester.Models.UserManagement;
using System.Collections.Generic;
using System.Linq;

namespace BarterMester.Models.Extensions
{
    public static class ExtensionMethods
    {
        public static IEnumerable<User> WithoutPasswords(this IEnumerable<User> users)
        {
            return users.Select(x => x.WithoutPassword());
        }

        public static User WithoutPassword(this User user)
        {
            user.PasswordHash = null;
            user.PasswordSalt = null;
            return user;
        }
    }
}
