﻿using BarterMester.Models.ItemManagement;
using BarterMester.Models.DemandManagement;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BarterMester.Models.UserManagement;

namespace BarterMester.Models.BarterManagement
{
    public class Barter
    {
        [Key]
        public Guid Id { get; set; }
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public User User { get; set; }
        [ForeignKey("ItemModel")]
        public Guid ItemModelId { get; set; }
        public ItemModel ItemModel { get; set; }
        [Column(TypeName = "decimal(18,3)")]
        public decimal BuyingPrice { get; set; }
        public DateTime StartedAt { get; set; }
        public DateTime? ClosedAt { get; set; }
        [StringLength(255)]
        public string Status { get; set; }
        [StringLength(255)]
        public string Comment { get; set; }
        [StringLength(255)]
        public string Location { get; set; }        
    }
}
