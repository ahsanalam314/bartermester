﻿namespace BarterMester.Models.BarterManagement
{
    public class CreateBarter
    {
        public string ItemModelId { get; set; }

        public string Comment { get; set; }

        public string Location { get; set; }
    }
}
