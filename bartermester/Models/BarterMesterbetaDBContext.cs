﻿using BarterMester.Models.BarterManagement;
using BarterMester.Models.ItemManagement;
using BarterMester.Models.DemandManagement;
using BarterMester.Models.Transaction;
using Microsoft.EntityFrameworkCore;
using BarterMester.Models.UserManagement;
using bartermester.Models.FooterManagement;
using bartermester.Models.CategoryColorManagement;

namespace BarterMester.Models.DBContext
{
    public class BarterMesterDBContext : DbContext
    {
        public BarterMesterDBContext(DbContextOptions options) : base(options) { }
        public DbSet<CategoryModel> CategoryModels { get; set; }
        public DbSet<ConditionModel> ConditionModels { get; set; }
        public DbSet<ItemModel> ItemModels { get; set; }
        public DbSet<ImageModel> ImageModels { get; set; }
        public DbSet<LocationModel> LocationModels { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Token> Tokens { get; set; }
        public DbSet<Barter> Barters { get; set; }
        public DbSet<TransactionFlow> TransactionFlows { get; set; }
        public DbSet<MessageModel> MessageModels { get; set; }
        public DbSet<StatusModel> StatusModels { get; set; }
        public DbSet<Demand> Demands { get; set; }
        public DbSet<UserCategoryColor> UserCategoryColors { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<OfferImage> OfferImages { get; set; }
        public DbSet<OfferStatus> OfferStatuses { get; set; }
        public DbSet<FooterModel> Footermodels { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder                
                .Entity<User>(x =>
                {
                    x.HasIndex(b => b.Email).IsUnique();
                })
                ;
        }
    }
}
