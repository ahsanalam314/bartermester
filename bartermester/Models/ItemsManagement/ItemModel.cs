﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BarterMester.Models.DemandManagement;
using System.Text.Json.Serialization;
using BarterMester.Models.UserManagement;

namespace BarterMester.Models.ItemManagement
{
    public class ItemModel
    {
        [Key]
        public Guid Id { get; set; }
        [StringLength(255)]
        public string Name { get; set; }
        [Column(TypeName = "decimal(18,3)")]
        public decimal SinglePrice { get; set; }
        public int UnitNumber { get; set; }
        [Column(TypeName = "decimal(18,3)")]
        public decimal Discount { get; set; }

        [ForeignKey("StatusModel")]
        public Guid StatusModelGuid { get; set; }
        public StatusModel StatusModel { get; set; }

        [StringLength(255)]
        public string Age { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        public string TransportModel { get; set; }

        public DateTime PostedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        [ForeignKey("LocationModel")]
        public Guid LocationModelId { get; set; }
        public LocationModel LocationModel {get; set;}

        [ForeignKey("ConditionModel")]
        public Guid ConditionModelId { get; set; }
        public ConditionModel ConditionModel { get; set; }

        [ForeignKey("CategoryModel")]
        public Guid CategoryModelId { get; set; }
        public CategoryModel CategoryModel { get; set; }
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public User User { get; set; }
        
        [JsonIgnore]
        public ICollection<ImageModel> ImageModels { get; set; }

        [JsonIgnore]
        public ICollection<MessageModel> MessageModels { get; set; }
    }
}
