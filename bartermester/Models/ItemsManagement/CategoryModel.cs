﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BarterMester.Models.ItemManagement
{
    public class CategoryModel
    {
        [Key]
        public Guid Id { get; set; }
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        public bool IsActive { get; set; }

        [JsonIgnore]
        public ICollection<ItemModel> ItemModels { get; set; }
    }
}
