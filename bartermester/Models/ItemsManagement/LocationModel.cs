﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BarterMester.Models.ItemManagement
{
    public class LocationModel
    {
        [Key]
        public Guid Id { get; set; }
        [StringLength(25)]
        public string City { get; set; }

        [JsonIgnore]
        public ICollection<ItemModel> ItemModels { get; set; }
    }
}
