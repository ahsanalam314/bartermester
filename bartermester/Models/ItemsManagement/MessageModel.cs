﻿using BarterMester.Models.DemandManagement;
using BarterMester.Models.UserManagement;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace BarterMester.Models.ItemManagement
{
    public class MessageModel
    {
        [Key]
        public Guid Id { get; set; }
        
        [ForeignKey("User")]
        public Guid BuyerId { get; set; }
        public User User { get; set; }

        public bool IsFromBuyer { get; set; }

        [ForeignKey("ItemModel")]
        [JsonIgnore]
        public Guid ItemModelId { get; set; }
        public ItemModel ItemModel { get; set; }

        [StringLength(255)]
        public string Body { get; set; }
        public DateTime PostedAt { get; set; }
        public bool IsNew { get; set; }
        public bool IsDeletedByBuyer { get; set; }
        public bool IsDeletedByItemOwner { get; set; }

    }
}
