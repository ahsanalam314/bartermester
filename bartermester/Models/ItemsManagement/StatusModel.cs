﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BarterMester.Models.ItemManagement
{
    public class StatusModel
    {
        public enum statusList { }

        [Key]
        public Guid Guid { get; set; }
        public int Id { get; set; }
        [StringLength(255)]
        public string Name { get; set; }
        
        [JsonIgnore]
        public ICollection<ItemModel> ItemModels { get; set; }
    }
}
