﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BarterMester.Models.ItemManagement
{
    public class CreateItemModel
    {
        public string Name { get; set; }
        [Column(TypeName = "decimal(18,3)")]
        public decimal SinglePrice { get; set; }
        public int UnitNumber { get; set; }
        [Column(TypeName = "decimal(18,3)")]
        public decimal Discount { get; set; }
        public string Age { get; set; }
        public string Description { get; set; }
        public string LocationModelId { get; set; }
        public string ConditionModelId { get; set; }
        public string CategoryModelId { get; set; }
        public string TransportModel { get; set; }
    }
}
