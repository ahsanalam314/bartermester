﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace BarterMester.Models.ItemManagement
{
    public class ImageModel
    {
        [Key]
        public Guid Id { get; set; }
        [StringLength(255)]
        public string Name { get; set; }
        public int SequenceId { get; set; }
        public DateTime PostedAt { get; set; }

        [ForeignKey("ItemModel")]
        [JsonIgnore]
        public Guid ItemModelId { get; set; }
        public ItemModel ItemModel { get; set; }

        [StringLength(255)]
        public string ImagePath { get; set; }
    }
}
