﻿using BarterMester.Models.DemandManagement;
using BarterMester.Models.BarterManagement;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BarterMester.Models.UserManagement;

namespace BarterMester.Models.Transaction
{
    public class TransactionFlow
    {
        [Key]
        public Guid Id { get; set; }
        [Column(TypeName = "decimal(18,3)")]
        public decimal Amount { get; set; }
        public TransactionType TransactionType { get; set; }

        [ForeignKey("User")]
        public Guid UserId { get; set; }        
        public User User { get; set; }

        public TransactionStatus TransactionStatus { get; set; }

        [ForeignKey("Barter")]
        public Guid BarterId { get; set; }
        public Barter Barter { get; set; }

        public DateTime TransactionAt { get; set; } //default huzza be az adott idot
    }
    [Flags]
    public enum TransactionType
    {
        Invite = 0,
        Barter = 1,
        Commission = 2,
        Spend = 3,
        Register = 4,
    }
    [Flags]
    public enum TransactionStatus
    {
        Nok = 0,
        Ok = 1,
        CallBack = 2
    }
}
