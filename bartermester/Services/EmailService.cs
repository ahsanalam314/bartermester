﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BarterMester.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using MailKit.Security;
using System.Net.Mail;
using System.Diagnostics;
using MimeKit;

namespace BarterMester.Services
{
    public interface IEmailService
    {
        SmtpClient _mailClient { get; set; }
        bool SendEmailAsync(string email, string subject, string message);
        bool SendPasswordReset(string email, string token);
    }
    public class EmailService : IEmailService
    {
        public SmtpClient _mailClient { get; set; }
        public EmailService()
        {
            SmtpClient smtpClient = new SmtpClient("smtp.office365.com");
            smtpClient.Port = 587;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential("peter.bereznay@bots2work.com", "Jakob133");
            smtpClient.EnableSsl = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.Timeout = 10000;
            this._mailClient = smtpClient;
        }

        public bool SendEmailAsync(string email, string subject, string message)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("no-reply.bartermester@bots2work.com");
                mail.To.Add(email);
                mail.Subject = subject;
                mail.Body = message;
                mail.IsBodyHtml = true;

                _mailClient.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }

        public bool SendPasswordReset(string email, string token)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("no-reply.bartermester@bots2work.com");
                mail.To.Add(email);
                mail.Subject = "Password Reset";
                TextPart textPart = new TextPart("html") {Text = "Change your password here: "};
                mail.Body = textPart.ToString();
                mail.IsBodyHtml = true;

                _mailClient.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }
    }
}
