﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using BarterMester.Models;
using BarterMester.Models.DemandManagement;
using System.Threading.Tasks;
using BarterMester.HelperCode;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using BarterMester.Models.UserManagement;
using BarterMester.Models.DBContext;

namespace BarterMester.Services
{
    public interface IUserService
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model);
        AuthenticateResponse Register(RegisterUserRequest model, Guid invitorId);
        IEnumerable<User> GetAll();
        User GetById(Guid id);
        Task InviteUser(string email, User user);
        Task SendPasswordChangeEmail(ResetPasswordEmailRequest model);
        Task ChangePassword(PasswordChangeRequest model, User user);
    }
    public class UserService : IUserService
    {
        private readonly BarterMesterDBContext _context;
        private readonly AppSettings _appSettings;
        private readonly IEmailService _emailService;

        public UserService(BarterMesterDBContext context, IOptions<AppSettings> appSettings, IEmailService emailService)
        {
            _context = context;
            _appSettings = appSettings.Value;
            _emailService = emailService;
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var user = _context.Users.SingleOrDefault(x => x.Username == model.Username);

            if (user == null) return null;

            if (VerifyPasswordHash(model.Password, user.PasswordHash, user.PasswordSalt))
            {
                var token = generateJwtToken(user, TokenType.LoginToken);
                return new AuthenticateResponse(user, token);
            }
            else
            {
                return null;
            }
        }
        public IEnumerable<User> GetAll()
        {
            return _context.Users;
        }

        public User GetById(Guid id)
        {
            return _context.Users.FirstOrDefault(x => x.Id == id);
        }
        public AuthenticateResponse Register(RegisterUserRequest model, Guid invitorId)
        {
            // validation
            if (string.IsNullOrWhiteSpace(model.Password))
                throw new AppException("Password is required");

            if (_context.Users.Any(x => x.Username == model.Username))
                throw new AppException("Username \"" + model.Username + "\" is already taken");

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(model.Password, out passwordHash, out passwordSalt);

            var newUser = _context.Users.Add(new User
            {
                Id = new Guid(),
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email.ToLower(),
                Username = model.Username,
                InvitorId = invitorId,
                Invitor = _context.Users.Find(invitorId),
                RegisteredAt = DateTime.Now,
                Role = "",
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt
            });

            var token = generateJwtToken(newUser.Entity, TokenType.LoginToken);

            _context.SaveChanges();

            return new AuthenticateResponse(newUser.Entity, token);
        }

        public async Task SendPasswordChangeEmail(ResetPasswordEmailRequest model)
        {
            var user = _context.Users.SingleOrDefault(x => x.Email == model.Email.ToLower());

            if (user == null) throw new Exception();

            byte[] passwordHash, passwordSalt;
            var newPass = GenerateRandomPassword();
            CreatePasswordHash(newPass, out passwordHash, out passwordSalt);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            _context.SaveChanges();
            _emailService.SendEmailAsync(user.Email, "PasswordReset", newPass);
        }

        public async Task ChangePassword(PasswordChangeRequest model, User user)
        {
            if (user == null) throw new Exception();

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(model.NewPassword, out passwordHash, out passwordSalt);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            await _context.SaveChangesAsync();
        }

        //Invite user
        public async Task InviteUser(string email, User user)
        {
            Guid tokenId = new Guid();
            var generatedToken = generateJwtToken(user, TokenType.InvitationToken);
            var tokenEntity = await _context.Tokens.AddAsync(new Token
            {
                Id = tokenId,
                TokenDetail = generatedToken,
                InvitorId = user.Id.ToString(),
                IsUsed = false,
                Email = email,
                CreatedAt = DateTime.Now,
            });
            await _context.SaveChangesAsync();
            string urlString = "https://bartermester.com/register?tokenId=" + tokenEntity.Entity.Id;

            _emailService.SendEmailAsync(email, "BarterMester meghívó",
                user.LastName + " " + user.FirstName + " meghívót küldött neked a <b>Bartermester</b> weboldalhoz."
                + "<br /><br /> Mi az a Bartermester?"
                + "<br /><br /> A Bartermester egy meghívásos alapon működő közösségi weboldal, ahol a tagok a piactér segítségével, a már nem szükséges vagy megunt dolgaikat tudják pontokra (Bartek) váltani.  A pontok csak a Bartermester weboldalon válthatók be bármilyen más termékre v. termékekre."
                + "<br /><br /> A BarterMester weboldalon nem történik valódi pénzforgalom, célja pedig egy olyan közösség megteremtése és bővitése, ahol a  háztartásokban a még használható dolgok kidobását és a felesleges fogyasztást igyekeznek csökkenteni."
                + "<br /><br /> Kellemes csereberélést!"
                + "<br /><br /> Link: " + urlString);
        }

        // helper methods
        private string generateJwtToken(User user, TokenType tokenType)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);

            DateTime expirationDate;

            switch (tokenType)
            {
                case TokenType.ForgotPasswordToken:
                    expirationDate = DateTime.Now.AddMinutes(30);
                    break;
                case TokenType.InvitationToken:
                    expirationDate = DateTime.Now.AddMonths(2);
                    break;
                case TokenType.LoginToken:
                    expirationDate = DateTime.Now.AddHours(24);
                    break;
                default:
                    expirationDate = DateTime.Now.AddHours(24);
                    break;
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()), new Claim("tokenType", tokenType.ToString()) }),
                Expires = expirationDate,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        public string GetCurrentTokenFromContext(HttpContext context)
        {
            var authString = context.Request.Headers.Where(j => j.Key == "Authorization").Select(j => j.Value).FirstOrDefault().ToString();
            return authString.Substring(7, authString.Length);
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }
            return true;
        }

        public static string GenerateRandomPassword()
        {
            var opts = new PasswordOptions()
            {
                RequiredLength = 8,
                RequiredUniqueChars = 4,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonAlphanumeric = true,
                RequireUppercase = true
            };

            string[] randomChars = new[] {
            "ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase 
            "abcdefghijkmnopqrstuvwxyz",    // lowercase
            "0123456789",                   // digits
            "!@$?_-"                        // non-alphanumeric
        };

            Random rand = new Random(Environment.TickCount);
            List<char> chars = new List<char>();

            if (opts.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (opts.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (opts.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (opts.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count; i < opts.RequiredLength
                || chars.Distinct().Count() < opts.RequiredUniqueChars; i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }
        public enum TokenType
        {
            ForgotPasswordToken,
            InvitationToken,
            LoginToken
        }
    }
}
