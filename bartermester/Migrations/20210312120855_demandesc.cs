﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BarterMester.Migrations
{
    public partial class demandesc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Demands",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Demands");
        }
    }
}
