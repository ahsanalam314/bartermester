﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BarterMester.Migrations
{
    public partial class productive_db : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CategoryModels",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryModels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ConditionModels",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConditionModels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LocationModels",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    City = table.Column<string>(maxLength: 25, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocationModels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StatusModels",
                columns: table => new
                {
                    Guid = table.Column<Guid>(nullable: false),
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatusModels", x => x.Guid);
                });

            migrationBuilder.CreateTable(
                name: "Tokens",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TokenDetail = table.Column<string>(maxLength: 255, nullable: true),
                    InvitorId = table.Column<string>(maxLength: 255, nullable: true),
                    IsUsed = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(maxLength: 255, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UsedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tokens", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 255, nullable: true),
                    LastName = table.Column<string>(maxLength: 255, nullable: true),
                    Email = table.Column<string>(maxLength: 255, nullable: true),
                    Username = table.Column<string>(maxLength: 255, nullable: true),
                    PasswordHash = table.Column<byte[]>(nullable: true),
                    PasswordSalt = table.Column<byte[]>(nullable: true),
                    RegisteredAt = table.Column<DateTime>(nullable: false),
                    Role = table.Column<string>(maxLength: 255, nullable: true),
                    InvitorId = table.Column<Guid>(nullable: false),
                    HasAcceptedGDPR = table.Column<bool>(nullable: false),
                    HasAcceptedTermsAndConditions = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Users_InvitorId",
                        column: x => x.InvitorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "ItemModels",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: true),
                    SinglePrice = table.Column<decimal>(type: "decimal(18,3)", nullable: false),
                    UnitNumber = table.Column<int>(nullable: false),
                    Discount = table.Column<decimal>(type: "decimal(18,3)", nullable: false),
                    StatusModelGuid = table.Column<Guid>(nullable: false),
                    Age = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    TransportModel = table.Column<string>(nullable: true),
                    PostedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    LocationModelId = table.Column<Guid>(nullable: false),
                    ConditionModelId = table.Column<Guid>(nullable: false),
                    CategoryModelId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemModels_CategoryModels_CategoryModelId",
                        column: x => x.CategoryModelId,
                        principalTable: "CategoryModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ItemModels_ConditionModels_ConditionModelId",
                        column: x => x.ConditionModelId,
                        principalTable: "ConditionModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ItemModels_LocationModels_LocationModelId",
                        column: x => x.LocationModelId,
                        principalTable: "LocationModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ItemModels_StatusModels_StatusModelGuid",
                        column: x => x.StatusModelGuid,
                        principalTable: "StatusModels",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ItemModels_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Barters",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    ItemModelId = table.Column<Guid>(nullable: false),
                    BuyingPrice = table.Column<decimal>(type: "decimal(18,3)", nullable: false),
                    StartedAt = table.Column<DateTime>(nullable: false),
                    ClosedAt = table.Column<DateTime>(nullable: true),
                    Status = table.Column<string>(maxLength: 255, nullable: true),
                    Comment = table.Column<string>(maxLength: 255, nullable: true),
                    Location = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Barters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Barters_ItemModels_ItemModelId",
                        column: x => x.ItemModelId,
                        principalTable: "ItemModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Barters_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "ImageModels",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: true),
                    SequenceId = table.Column<int>(nullable: false),
                    PostedAt = table.Column<DateTime>(nullable: false),
                    ItemModelId = table.Column<Guid>(nullable: false),
                    ImagePath = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImageModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ImageModels_ItemModels_ItemModelId",
                        column: x => x.ItemModelId,
                        principalTable: "ItemModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "MessageModels",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    BuyerId = table.Column<Guid>(nullable: false),
                    IsFromBuyer = table.Column<bool>(nullable: false),
                    ItemModelId = table.Column<Guid>(nullable: false),
                    Body = table.Column<string>(maxLength: 255, nullable: true),
                    PostedAt = table.Column<DateTime>(nullable: false),
                    IsNew = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessageModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MessageModels_Users_BuyerId",
                        column: x => x.BuyerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_MessageModels_ItemModels_ItemModelId",
                        column: x => x.ItemModelId,
                        principalTable: "ItemModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "TransactionFlows",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,3)", nullable: false),
                    TransactionType = table.Column<int>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    TransactionStatus = table.Column<int>(nullable: false),
                    BarterId = table.Column<Guid>(nullable: false),
                    TransactionAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionFlows", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionFlows_Barters_BarterId",
                        column: x => x.BarterId,
                        principalTable: "Barters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_TransactionFlows_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Barters_ItemModelId",
                table: "Barters",
                column: "ItemModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Barters_UserId",
                table: "Barters",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ImageModels_ItemModelId",
                table: "ImageModels",
                column: "ItemModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemModels_CategoryModelId",
                table: "ItemModels",
                column: "CategoryModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemModels_ConditionModelId",
                table: "ItemModels",
                column: "ConditionModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemModels_LocationModelId",
                table: "ItemModels",
                column: "LocationModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemModels_StatusModelGuid",
                table: "ItemModels",
                column: "StatusModelGuid");

            migrationBuilder.CreateIndex(
                name: "IX_ItemModels_UserId",
                table: "ItemModels",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_MessageModels_BuyerId",
                table: "MessageModels",
                column: "BuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_MessageModels_ItemModelId",
                table: "MessageModels",
                column: "ItemModelId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFlows_BarterId",
                table: "TransactionFlows",
                column: "BarterId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFlows_UserId",
                table: "TransactionFlows",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Email",
                table: "Users",
                column: "Email",
                unique: true,
                filter: "[Email] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Users_InvitorId",
                table: "Users",
                column: "InvitorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ImageModels");

            migrationBuilder.DropTable(
                name: "MessageModels");

            migrationBuilder.DropTable(
                name: "Tokens");

            migrationBuilder.DropTable(
                name: "TransactionFlows");

            migrationBuilder.DropTable(
                name: "Barters");

            migrationBuilder.DropTable(
                name: "ItemModels");

            migrationBuilder.DropTable(
                name: "CategoryModels");

            migrationBuilder.DropTable(
                name: "ConditionModels");

            migrationBuilder.DropTable(
                name: "LocationModels");

            migrationBuilder.DropTable(
                name: "StatusModels");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
