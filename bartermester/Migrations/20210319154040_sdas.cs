﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BarterMester.Migrations
{
    public partial class sdas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TransportModel",
                table: "Offers",
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TransportModel",
                table: "Offers");
        }
    }
}
