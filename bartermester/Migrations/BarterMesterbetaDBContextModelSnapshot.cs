﻿// <auto-generated />
using System;
using BarterMester.Models.DBContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace BarterMester.Migrations
{
    [DbContext(typeof(BarterMesterDBContext))]
    partial class BarterMesterbetaDBContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.6")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("BarterMester.Models.BarterManagement.Barter", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<decimal>("BuyingPrice")
                        .HasColumnType("decimal(18,3)");

                    b.Property<DateTime?>("ClosedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Comment")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<Guid>("ItemModelId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Location")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<DateTime>("StartedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Status")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("ItemModelId");

                    b.HasIndex("UserId");

                    b.ToTable("Barters");
                });

            modelBuilder.Entity("BarterMester.Models.DemandManagement.Demand", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("CategoryModelId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<DateTime>("PostedAt")
                        .HasColumnType("datetime2");

                    b.Property<Guid>("StatusModelGuid")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("datetime2");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("CategoryModelId");

                    b.HasIndex("StatusModelGuid");

                    b.HasIndex("UserId");

                    b.ToTable("Demands");
                });

            modelBuilder.Entity("BarterMester.Models.DemandManagement.Offer", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<decimal>("Amount")
                        .HasColumnType("decimal(18,3)");

                    b.Property<Guid>("DemandId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<Guid?>("LocationModelId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<Guid>("OfferStatusGuid")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("PostedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("TransportModel")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("datetime2");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("DemandId");

                    b.HasIndex("LocationModelId");

                    b.HasIndex("OfferStatusGuid");

                    b.HasIndex("UserId");

                    b.ToTable("Offers");
                });

            modelBuilder.Entity("BarterMester.Models.DemandManagement.OfferImage", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("ImagePath")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<Guid>("OfferId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("PostedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("OfferId");

                    b.ToTable("OfferImages");
                });

            modelBuilder.Entity("BarterMester.Models.DemandManagement.OfferStatus", b =>
                {
                    b.Property<Guid>("Guid")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.HasKey("Guid");

                    b.ToTable("OfferStatuses");
                });

            modelBuilder.Entity("BarterMester.Models.ItemManagement.CategoryModel", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<bool>("IsActive")
                        .HasColumnType("bit");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("CategoryModels");
                });

            modelBuilder.Entity("BarterMester.Models.ItemManagement.ConditionModel", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("IsActive")
                        .HasColumnType("bit");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("ConditionModels");
                });

            modelBuilder.Entity("BarterMester.Models.ItemManagement.ImageModel", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("ImagePath")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<Guid>("ItemModelId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<DateTime>("PostedAt")
                        .HasColumnType("datetime2");

                    b.Property<int>("SequenceId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("ItemModelId");

                    b.ToTable("ImageModels");
                });

            modelBuilder.Entity("BarterMester.Models.ItemManagement.ItemModel", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Age")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<Guid>("CategoryModelId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("ConditionModelId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<decimal>("Discount")
                        .HasColumnType("decimal(18,3)");

                    b.Property<Guid>("LocationModelId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<DateTime>("PostedAt")
                        .HasColumnType("datetime2");

                    b.Property<decimal>("SinglePrice")
                        .HasColumnType("decimal(18,3)");

                    b.Property<Guid>("StatusModelGuid")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("TransportModel")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("UnitNumber")
                        .HasColumnType("int");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("datetime2");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("CategoryModelId");

                    b.HasIndex("ConditionModelId");

                    b.HasIndex("LocationModelId");

                    b.HasIndex("StatusModelGuid");

                    b.HasIndex("UserId");

                    b.ToTable("ItemModels");
                });

            modelBuilder.Entity("BarterMester.Models.ItemManagement.LocationModel", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("City")
                        .HasColumnType("nvarchar(25)")
                        .HasMaxLength(25);

                    b.HasKey("Id");

                    b.ToTable("LocationModels");
                });

            modelBuilder.Entity("BarterMester.Models.ItemManagement.MessageModel", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Body")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<Guid>("BuyerId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("IsDeletedByBuyer")
                        .HasColumnType("bit");

                    b.Property<bool>("IsDeletedByItemOwner")
                        .HasColumnType("bit");

                    b.Property<bool>("IsFromBuyer")
                        .HasColumnType("bit");

                    b.Property<bool>("IsNew")
                        .HasColumnType("bit");

                    b.Property<Guid>("ItemModelId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("PostedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("BuyerId");

                    b.HasIndex("ItemModelId");

                    b.ToTable("MessageModels");
                });

            modelBuilder.Entity("BarterMester.Models.ItemManagement.StatusModel", b =>
                {
                    b.Property<Guid>("Guid")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.HasKey("Guid");

                    b.ToTable("StatusModels");
                });

            modelBuilder.Entity("BarterMester.Models.Transaction.TransactionFlow", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<decimal>("Amount")
                        .HasColumnType("decimal(18,3)");

                    b.Property<Guid>("BarterId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("TransactionAt")
                        .HasColumnType("datetime2");

                    b.Property<int>("TransactionStatus")
                        .HasColumnType("int");

                    b.Property<int>("TransactionType")
                        .HasColumnType("int");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("BarterId");

                    b.HasIndex("UserId");

                    b.ToTable("TransactionFlows");
                });

            modelBuilder.Entity("BarterMester.Models.UserManagement.Token", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<string>("InvitorId")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<bool>("IsUsed")
                        .HasColumnType("bit");

                    b.Property<string>("TokenDetail")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<DateTime?>("UsedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.ToTable("Tokens");
                });

            modelBuilder.Entity("BarterMester.Models.UserManagement.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<bool>("HasAcceptedGDPR")
                        .HasColumnType("bit");

                    b.Property<bool>("HasAcceptedTermsAndConditions")
                        .HasColumnType("bit");

                    b.Property<Guid>("InvitorId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<byte[]>("PasswordHash")
                        .HasColumnType("varbinary(max)");

                    b.Property<byte[]>("PasswordSalt")
                        .HasColumnType("varbinary(max)");

                    b.Property<DateTime>("RegisteredAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Role")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.Property<string>("Username")
                        .HasColumnType("nvarchar(255)")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("Email")
                        .IsUnique()
                        .HasFilter("[Email] IS NOT NULL");

                    b.HasIndex("InvitorId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("BarterMester.Models.BarterManagement.Barter", b =>
                {
                    b.HasOne("BarterMester.Models.ItemManagement.ItemModel", "ItemModel")
                        .WithMany()
                        .HasForeignKey("ItemModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("BarterMester.Models.UserManagement.User", "User")
                        .WithMany("Barters")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("BarterMester.Models.DemandManagement.Demand", b =>
                {
                    b.HasOne("BarterMester.Models.ItemManagement.CategoryModel", "CategoryModel")
                        .WithMany()
                        .HasForeignKey("CategoryModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("BarterMester.Models.ItemManagement.StatusModel", "StatusModel")
                        .WithMany()
                        .HasForeignKey("StatusModelGuid")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("BarterMester.Models.UserManagement.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("BarterMester.Models.DemandManagement.Offer", b =>
                {
                    b.HasOne("BarterMester.Models.DemandManagement.Demand", "Demand")
                        .WithMany("Offers")
                        .HasForeignKey("DemandId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("BarterMester.Models.ItemManagement.LocationModel", "LocationModel")
                        .WithMany()
                        .HasForeignKey("LocationModelId");

                    b.HasOne("BarterMester.Models.DemandManagement.OfferStatus", "OfferStatus")
                        .WithMany("Offers")
                        .HasForeignKey("OfferStatusGuid")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("BarterMester.Models.UserManagement.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("BarterMester.Models.DemandManagement.OfferImage", b =>
                {
                    b.HasOne("BarterMester.Models.DemandManagement.Offer", "Offer")
                        .WithMany("OfferImages")
                        .HasForeignKey("OfferId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("BarterMester.Models.ItemManagement.ImageModel", b =>
                {
                    b.HasOne("BarterMester.Models.ItemManagement.ItemModel", "ItemModel")
                        .WithMany("ImageModels")
                        .HasForeignKey("ItemModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("BarterMester.Models.ItemManagement.ItemModel", b =>
                {
                    b.HasOne("BarterMester.Models.ItemManagement.CategoryModel", "CategoryModel")
                        .WithMany("ItemModels")
                        .HasForeignKey("CategoryModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("BarterMester.Models.ItemManagement.ConditionModel", "ConditionModel")
                        .WithMany("ItemModels")
                        .HasForeignKey("ConditionModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("BarterMester.Models.ItemManagement.LocationModel", "LocationModel")
                        .WithMany("ItemModels")
                        .HasForeignKey("LocationModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("BarterMester.Models.ItemManagement.StatusModel", "StatusModel")
                        .WithMany("ItemModels")
                        .HasForeignKey("StatusModelGuid")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("BarterMester.Models.UserManagement.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("BarterMester.Models.ItemManagement.MessageModel", b =>
                {
                    b.HasOne("BarterMester.Models.UserManagement.User", "User")
                        .WithMany()
                        .HasForeignKey("BuyerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("BarterMester.Models.ItemManagement.ItemModel", "ItemModel")
                        .WithMany("MessageModels")
                        .HasForeignKey("ItemModelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("BarterMester.Models.Transaction.TransactionFlow", b =>
                {
                    b.HasOne("BarterMester.Models.BarterManagement.Barter", "Barter")
                        .WithMany()
                        .HasForeignKey("BarterId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("BarterMester.Models.UserManagement.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("BarterMester.Models.UserManagement.User", b =>
                {
                    b.HasOne("BarterMester.Models.UserManagement.User", "Invitor")
                        .WithMany()
                        .HasForeignKey("InvitorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
