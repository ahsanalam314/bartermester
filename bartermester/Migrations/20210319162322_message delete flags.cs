﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BarterMester.Migrations
{
    public partial class messagedeleteflags : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeletedByBuyer",
                table: "MessageModels",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeletedByItemOwner",
                table: "MessageModels",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeletedByBuyer",
                table: "MessageModels");

            migrationBuilder.DropColumn(
                name: "IsDeletedByItemOwner",
                table: "MessageModels");
        }
    }
}
