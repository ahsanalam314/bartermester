﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BarterMester.Models;
using BarterMester.Models.ItemManagement;
using BarterMester.HelperCode;
using static BarterMester.Services.UserService;
using System.Linq;
using BarterMester.Models.DBContext;

namespace BarterMester.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConditionModelsController : ControllerBase
    {
        private readonly BarterMesterDBContext _context;

        public ConditionModelsController(BarterMesterDBContext context)
        {
            _context = context;
        }
        [Authorize(TokenType.LoginToken)]
        // GET: api/ConditionModels/GetAllCondition
        [Route("GetAllCondition")]
        public async Task<ActionResult<IEnumerable<ConditionModel>>> GetAllCondition()
        {
            return await _context.ConditionModels
                .Where(x => x.IsActive == true)
                .ToListAsync();
        }
    }
}
