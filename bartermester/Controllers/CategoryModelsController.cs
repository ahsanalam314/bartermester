﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BarterMester.Models;
using BarterMester.Models.ItemManagement;
using BarterMester.HelperCode;
using static BarterMester.Services.UserService;
using System.Linq;
using BarterMester.Models.DBContext;

namespace BarterMester.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryModelsController : ControllerBase
    {
        private readonly BarterMesterDBContext _context;

        public CategoryModelsController(BarterMesterDBContext context)
        {
            _context = context;
        }
        [Authorize(TokenType.LoginToken)]
        // GET: api/CategoryModels/GetAllCategory
        [Route("GetAllCategory")]
        public async Task<ActionResult<IEnumerable<CategoryModel>>> GetAllCategory()
        {
            return await _context.CategoryModels
                .Where(x => x.IsActive == true)
                .ToListAsync();
        }
    }
}
