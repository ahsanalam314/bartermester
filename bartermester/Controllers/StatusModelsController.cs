﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BarterMester.Models;
using BarterMester.Models.ItemManagement;
using BarterMester.HelperCode;
using static BarterMester.Services.UserService;
using BarterMester.Models.DBContext;

namespace BarterMester.Controllers
{
    [Authorize(TokenType.LoginToken)]
    [Route("api/[controller]")]
    [ApiController]
    public class StatusModelsController : ControllerBase
    {
        private readonly BarterMesterDBContext _context;

        public StatusModelsController(BarterMesterDBContext context)
        {
            _context = context;
        }

        // GET: api/StatusModels/GetAllStatus
        [Route("GetAllStatus")]
        public async Task<ActionResult<IEnumerable<StatusModel>>> GetAllStatus()
        {
            return await _context.StatusModels.ToListAsync();
        }
    }
}
