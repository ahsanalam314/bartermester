﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BarterMester.Models;
using BarterMester.Models.ItemManagement;
using Microsoft.AspNetCore.Hosting;
using BarterMester.HelperCode;
using Microsoft.AspNetCore.Http;
using System.IO;
using static BarterMester.Services.UserService;
using BarterMester.Services;
using BarterMester.Models.DBContext;
using bartermester.Models.CategoryColorManagement;

namespace bartermester.Controllers
{
    public class CategoryColorModelsController : ControllerBase
    {

        #region defaultControllerConfig
        private readonly BarterMesterDBContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IEmailService _emailservice;




        [Route("GetCategoryColorbyuserid")]
        public async Task<ActionResult<IEnumerable<UserCategoryColor>>> GetCategoryColorbyuserid(Guid userid)
        {
            try
            {
                return await _context.UserCategoryColors
                    .Where(x => x.UserId == userid)
                    .OrderBy(x => x.Id)

                    .ToListAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        // SET: api/CategoryColorModels/SetUserCategoryColor

        [Route("SetUserCategoryColor")]
        public async Task<ActionResult<UserCategoryColor>> SetUserCategoryColor([FromBody] UserCategoryColorDto categorycolorDto)
        {
            try
            {
                var result = await _context.UserCategoryColors.AddAsync(new UserCategoryColor
                {
                    Id = Guid.NewGuid(),
                    color = categorycolorDto.color,                
                    //PostedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    categoryId = Guid.Parse(categorycolorDto.categoryId),
                    UserId = (Guid)HttpContext.Items["UserId"]
                });

                await _context.SaveChangesAsync();
                return Ok(result.Entity);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        // SET: api/CategoryColorModels/UpdateUserCategoryColor
        [Route("UpdateUserCategoryColor")]
        public async Task<IActionResult> UpdateUserCategoryColor(Guid id, Guid categoryId, Guid UserId ,string color)
        {
            try
            {
                var colorcategory = _context.UserCategoryColors.Find(id);
                if (colorcategory == null)
                {
                    return NotFound();
                }
                else
                {
                    //Guid newStatusGuid = GetStatusGuidByInt(newStatusId);
                    //if (demand.StatusModelGuid != newStatusGuid)
                    //{ 
                    //    demand.UpdatedAt = DateTime.Now;
                    //    demand.StatusModelGuid = newStatusGuid;
                    //    await _context.SaveChangesAsync();
                    //}
                    //return Ok();
                    colorcategory.UpdatedAt = DateTime.Now;
                    _context.UserCategoryColors.Update(colorcategory);
                    await _context.SaveChangesAsync();
                   return Ok();

                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }




        #endregion
    }
}
