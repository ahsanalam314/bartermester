﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BarterMester.Models;
using BarterMester.Models.ItemManagement;
using BarterMester.HelperCode;
using static BarterMester.Services.UserService;
using System.IO;
using BarterMester.Services;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using BarterMester.Models.DBContext;

namespace BarterMester.Controllers
{
    [Authorize(TokenType.LoginToken)]
    [Route("api/[controller]")]
    [ApiController]
    public class ImageModelsController : ControllerBase
    {
        private readonly BarterMesterDBContext _context;
        private readonly IUserService _userservice;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ImageModelsController(BarterMesterDBContext context, IUserService userservice, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _userservice = userservice;
            _webHostEnvironment = webHostEnvironment;
        }

        [Route("GetImagesbyItem")]
        public async Task<ActionResult<IEnumerable<ImageModel>>> GetImagesbyItem(Guid itemId)
        {
            try
            {
                return await _context.ImageModels
                    .Where(x => x.ItemModelId == itemId)
                    .OrderBy(x => x.SequenceId)

                    .ToListAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        //Get first pic of each Item per user
        [Route("GetImagesbyUser")]
        public async Task<ActionResult<IEnumerable<ImageModel>>> GetImagesbyUser()
        {
            try
            {
                return await _context.ImageModels
                    .Include(x => x.ItemModel)
                    .Where(x => x.ItemModel.UserId == (Guid)HttpContext.Items["UserId"])
                    .Where(x => x.SequenceId == 1)
                    .OrderBy(x => x.SequenceId)                    
                    .ToListAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [Route("UploadPicture")]
        public async Task<ActionResult> UploadPicture(Guid itemId)
        {
            try
            {
                var countExistingFiles = _context.ImageModels.Where(x => x.ItemModelId == itemId).Count();
                var files = Request.Form.Files;
                var folderName = Path.Combine("StaticFiles", "Images");
                var pathToSave = Path.Combine(_webHostEnvironment.WebRootPath, folderName);
                try
                {
                    if (files.Count > 0)
                    {
                        int i = countExistingFiles + 1;
                        foreach (var file in files)
                        {                           
                            //var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                            var fileName = Guid.NewGuid();
                            var fullPath = Path.Combine(pathToSave, fileName.ToString() + ".jpg");
                            var dbPath = Path.Combine(folderName, fileName.ToString() + ".jpg");

                            //If the file already exists, an System.IO.IOException exception is thrown
                            using (var stream = new FileStream(fullPath, FileMode.CreateNew))
                            {
                                file.CopyTo(stream);
                            }

                            var result = await _context.ImageModels.AddAsync(new ImageModel
                            {
                                Id = fileName,
                                Name = fileName.ToString() + ".jpg",
                                ImagePath = dbPath,
                                ItemModelId = itemId,
                                SequenceId = i,
                                PostedAt = DateTime.Now
                            });

                            await _context.SaveChangesAsync();
                            i += 1;
                        }
                        return Ok();
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                catch (Exception e)
                {
                    return BadRequest($"Error in write image data to db: {e}");
                }
            }
            catch (Exception e)
            {
                if (e.InnerException.ToString() == "System.IO.IOException")
                {
                    return BadRequest($"Filename already exist: {e}");
                }
                return BadRequest($"Internal server error: {e}");
            }
        }
    }
}
