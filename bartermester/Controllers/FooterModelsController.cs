﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BarterMester.Models.ItemManagement;
using Microsoft.AspNetCore.Hosting;
using BarterMester.HelperCode;
using Microsoft.AspNetCore.Http;
using System.IO;
using static BarterMester.Services.UserService;
using BarterMester.Services;
using BarterMester.Models.DBContext;
using BarterMester.Models.UserManagement;
using bartermester.Models.FooterManagement;

namespace Bartermester.Controllers
{
    [Authorize(TokenType.LoginToken)]
    [Route("api/[controller]")]
    [ApiController]
    public class FooterModelsController : ControllerBase
    {
        #region defaultControllerConfig
        private readonly BarterMesterDBContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IEmailService _emailservice;


        // GET: api/FooterModels/GetAllFooter
        [Route("GetAllFooter")]
        public async Task<ActionResult<IEnumerable<FooterModel>>> GetAllFooter()
        {
            return await _context.Footermodels
                .OrderBy(x => x.text)
                .ToListAsync();
        }



        // SET: api/FooterModels/SetNewFooter
        [Route("SetNewFooter")]
        public async Task<ActionResult<FooterModel>> SetNewFooter([FromBody] FooterDto footerDto)
        {
            try
            {
                var result = await _context.Footermodels.AddAsync(new FooterModel
                {
                    Id = Guid.NewGuid(),
                    text = footerDto.text,
                    //UserId = (Guid)HttpContext.Items["UserId"]

                });

                await _context.SaveChangesAsync();
                return Ok(result.Entity);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // SET: api/FooterModels/UpdateFooter
        [Route("UpdateFooter")]
        public async Task<IActionResult> UpdateFooter(Guid id, string name)
        {
            try
            {
                var fitemmodel = _context.Footermodels.Find(id);
                if (fitemmodel == null)
                {
                    return NotFound();
                }
                  else
                {
                    fitemmodel.UpdatedAt = DateTime.Now;
                    fitemmodel.text = name; 
                    await _context.SaveChangesAsync();

                    return Ok();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        #endregion

    }
}
