﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BarterMester.HelperCode;
using BarterMester.Models;
using BarterMester.Models.ItemManagement;
using BarterMester.Models.DemandManagement;
using BarterMester.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static BarterMester.Services.UserService;
using BarterMester.Models.DBContext;
using BarterMester.Models.UserManagement;

namespace bartermester.Controllers
{
    [Authorize(TokenType.LoginToken)]
    [Route("api/[controller]")]
    [ApiController]
    public class SharedController : ControllerBase
    {
        private IEmailService _emailservice;
        private readonly BarterMesterDBContext _context;

        public SharedController(IEmailService emailservice, BarterMesterDBContext context)
        {
            _context = context;
            _emailservice = emailservice;
        }

        [Route("InMailAdmin")]
        public async Task<ActionResult<MessageModel>> InMailAdmin(IList<User> users, string body)
        {
            try
            {
                foreach (User user in users)
                {
                    await _context.MessageModels.AddAsync(new MessageModel
                    {
                        Id = Guid.NewGuid(),
                        PostedAt = DateTime.Now,
                        BuyerId = user.Id,
                        IsFromBuyer = false,
                        ItemModelId = Guid.Parse("00000000-0000-0000-0000-000000000000"),
                        Body = body,
                        IsNew = true
                    });
                    _context.SaveChanges();
                    _emailservice.SendEmailAsync(user.Email, "Admin üzeneted érkezett!", "Üzenetek megtekintése: https://bartermester.com/BackPack/ListMessage");
                }
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [Route("EmailAdmin")]
        public void EmailAdmin(IList<User> users, string body)
        {
            foreach (User user in users)
            {
                _emailservice.SendEmailAsync(user.Email, "Admin üzeneted érkezett!", body);
            }
        }



    }
}