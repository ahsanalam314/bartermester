﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BarterMester.Models.BarterManagement;
using BarterMester.Models.Transaction;
using BarterMester.HelperCode;
using BarterMester.Models.ItemManagement;
using Microsoft.AspNetCore.Http;
using static BarterMester.Services.UserService;
using BarterMester.Services;
using BarterMester.Models.UserManagement;
using BarterMester.Models.DBContext;

namespace BarterMester.Controllers
{
    [Authorize(TokenType.LoginToken)]
    [Route("api/[controller]")]
    [ApiController]
    public class BarterModelsController : ControllerBase
    {
        private readonly BarterMesterDBContext _context;
        private readonly IEmailService _emailservice;

        public BarterModelsController(BarterMesterDBContext context, IEmailService emailservice)
        {
            _context = context;
            _emailservice = emailservice;
        }

        // POST: api/BarterModels
        [Route("CreateBarter")]
        public async Task<ActionResult<IEnumerable<Barter>>> CreateBarter([FromBody] CreateBarter createBarter, int newStatusId)
        {
            try
            {
                var firstBarter = _context.Barters.Where(x => x.UserId == (Guid)HttpContext.Items["UserId"]).FirstOrDefault();

                Guid barterId = Guid.NewGuid();
                ItemModel itemModelUsed = _context.ItemModels.Find(Guid.Parse(createBarter.ItemModelId));
                User seller = _context.Users.Find(itemModelUsed.UserId);
                Guid statusId = GetStatusGuidByInt(newStatusId);

                var result = await _context.Barters.AddAsync(new Barter
                {
                    Id = barterId,
                    UserId = (Guid)HttpContext.Items["UserId"],
                    ItemModelId = itemModelUsed.Id,
                    BuyingPrice = itemModelUsed.SinglePrice,
                    StartedAt = DateTime.Now,
                    Status = "Completed",
                    Location = createBarter.Location,
                    ClosedAt = DateTime.Now,
                    Comment = createBarter.Comment
                });
                _context.SaveChanges();

                if (firstBarter == null)
                {
                    await CreateTransaction(result.Entity, TransactionType.Invite);
                }

                if (itemModelUsed.StatusModelGuid != statusId)
                {
                    itemModelUsed.UpdatedAt = DateTime.Now;
                    itemModelUsed.StatusModelGuid = statusId;
                    _context.SaveChanges();
                }

                //Create transactions
                await CreateTransaction(result.Entity, TransactionType.Spend);
                await CreateTransaction(result.Entity, TransactionType.Barter);
                await CreateTransaction(result.Entity, TransactionType.Commission);
                //Create automatic message
                await CreateDefaultSoldMessage(result.Entity.ItemModelId);
                _emailservice.SendEmailAsync(seller.Email, "Becserélték egy terméked!", "Üzenetek megtekintése: https://bartermester.com/BackPack/ListMessage");
                await _context.SaveChangesAsync();
                return Ok(result.Entity);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        //Create default Sold Message
        [Route("CreateDefaultSoldMessage")]
        public async Task<IActionResult> CreateDefaultSoldMessage(Guid itemModelId)
        {
            try
            {
                var newMessage = _context.MessageModels.AddAsync(new MessageModel
                {
                    Id = new Guid(),
                    Body = "Adásvétel emlékeztető",
                    PostedAt = DateTime.Now,
                    IsNew = true,
                    ItemModelId = itemModelId,
                    BuyerId = (Guid)HttpContext.Items["UserId"],
                    IsFromBuyer = true,
                });

                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // GET: api/BarterModels
        [Route("GetBalance")]
        public async Task<ActionResult<IEnumerable<TransactionFlow>>> GetBalance()
        {
            try
            {
                return await _context.TransactionFlows
                    .Include(x => x.Barter)
                    .Include(y => y.User)
                    .Where(m => m.UserId == (Guid)HttpContext.Items["UserId"])
                    .ToListAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }


        //Create transaction
        [Route("CreateTransaction")]
        public async Task<IActionResult> CreateTransaction(Barter barter, TransactionType? transactionType)
        {
            try
            {
                User user = (User)HttpContext.Items["User"];
                switch (transactionType)
                {
                    case TransactionType.Invite:
                        var newTransaction = _context.TransactionFlows.AddAsync(new TransactionFlow
                        {
                            Id = new Guid(),
                            Amount = 500,
                            TransactionType = TransactionType.Invite,
                            UserId = user.InvitorId,
                            TransactionStatus = TransactionStatus.Ok,
                            BarterId = barter.Id,
                            TransactionAt = DateTime.Now
                        });
                        break;

                    case TransactionType.Barter:
                        newTransaction = _context.TransactionFlows.AddAsync(new TransactionFlow
                        {
                            Id = new Guid(),
                            Amount = barter.BuyingPrice,
                            TransactionType = TransactionType.Barter,
                            UserId = barter.ItemModel.UserId,
                            TransactionStatus = TransactionStatus.Ok,
                            BarterId = barter.Id,
                            TransactionAt = DateTime.Now
                        });
                        break;

                    case TransactionType.Commission:
                        var fbarter = _context.Barters
                    .Include(x => x.ItemModel)
                    .ThenInclude(x => x.User)
                    .FirstOrDefault(x => x.Id == barter.Id);

                        newTransaction = _context.TransactionFlows.AddAsync(new TransactionFlow
                        {
                            Id = new Guid(),
                            Amount = barter.BuyingPrice * 2 / 100,
                            TransactionType = TransactionType.Commission,
                            UserId = fbarter.ItemModel.User.InvitorId,
                            TransactionStatus = TransactionStatus.Ok,
                            BarterId = barter.Id,
                            TransactionAt = DateTime.Now
                        });
                        break;

                    case TransactionType.Spend:
                        newTransaction = _context.TransactionFlows.AddAsync(new TransactionFlow
                        {
                            Id = new Guid(),
                            Amount = -1 * barter.BuyingPrice,
                            TransactionType = TransactionType.Spend,
                            UserId = barter.UserId,
                            TransactionStatus = TransactionStatus.Ok,
                            BarterId = barter.Id,
                            TransactionAt = DateTime.Now
                        });
                        break;

                    default:
                        break;
                }
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // DELETE: api/BarterModels/5
        [Route("DeleteBarter")]
        public async Task<ActionResult<Barter>> DeleteBarter(Guid id)
        {
            var barter = await _context.Barters.FindAsync(id);
            if (barter == null)
            {
                return NotFound();
            }

            _context.Barters.Remove(barter);
            await _context.SaveChangesAsync();

            return barter;
        }
        public Guid GetStatusGuidByInt(int intId)
        { 
            return _context.StatusModels.Where(a => a.Id == intId).FirstOrDefault().Guid;
        }
    }
}
