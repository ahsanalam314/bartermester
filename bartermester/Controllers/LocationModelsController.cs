﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BarterMester.Models;
using BarterMester.Models.ItemManagement;
using BarterMester.HelperCode;
using static BarterMester.Services.UserService;
using System.Linq;
using BarterMester.Models.DBContext;

namespace BarterMester.Controllers
{
    [Authorize(TokenType.LoginToken)]
    [Route("api/[controller]")]
    [ApiController]
    public class LocationModelsController : ControllerBase
    {
        private readonly BarterMesterDBContext _context;

        public LocationModelsController(BarterMesterDBContext context)
        {
            _context = context;
        }

        // GET: api/LocationModels/GetAllLocation
        [Route("GetAllLocation")]
        public async Task<ActionResult<IEnumerable<LocationModel>>> GetAllLocation()
        {
            return await _context.LocationModels                
                .OrderBy(x => x.City)
                .ToListAsync();
        }
    }
}
