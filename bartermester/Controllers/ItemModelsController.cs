﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BarterMester.Models.ItemManagement;
using Microsoft.AspNetCore.Hosting;
using BarterMester.HelperCode;
using Microsoft.AspNetCore.Http;
using System.IO;
using static BarterMester.Services.UserService;
using BarterMester.Services;
using BarterMester.Models.DBContext;
using BarterMester.Models.UserManagement;

namespace BarterMester.Controllers
{
    [Authorize(TokenType.LoginToken)]
    [Route("api/[controller]")]
    [ApiController]
    public class ItemModelsController : ControllerBase
    {
        private readonly BarterMesterDBContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IEmailService _emailservice;

        public ItemModelsController(BarterMesterDBContext context, IWebHostEnvironment webHostEnvironment, IEmailService emailservice)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
            _emailservice = emailservice;
        }

        // GET: api/ItemModels
        [Route("GetAllActiveItem")]
        public async Task<ActionResult<IEnumerable<ItemModel>>> GetAllActiveItem()
        {
            try
            {
                return await _context.ItemModels
                    .Include(x => x.CategoryModel)
                    .Include(y => y.ConditionModel)
                    .Include(z => z.LocationModel)
                    .Include(s => s.StatusModel)
                    .Include(b => b.User)
                    .Where(m => m.StatusModel.Id == 1)
                    .OrderByDescending(x => x.PostedAt)
                    .ToListAsync();

            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // GET: api/ItemModels
        [Route("GetItemByUser")]
        public async Task<ActionResult<IEnumerable<ItemModel>>> GetItemByUser()
        {
            try
            {
                return await _context.ItemModels
                    .Include(x => x.CategoryModel)
                    .Include(y => y.ConditionModel)
                    .Include(z => z.LocationModel)
                    .Include(s => s.StatusModel)
                    .Include(b => b.User)
                    .Where(m => m.UserId == (Guid)HttpContext.Items["UserId"])
                    .OrderBy(d => d.StatusModel.Id)
                    .ThenBy(d => d.Name)
                    .ToListAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // PUT: api/ItemModels
        [Route("ModifyItem")]
        public async Task<ActionResult<IEnumerable<ItemModel>>> ModifyItem([FromBody] ItemModel itemModel)
        {
            try
            {
                itemModel.UpdatedAt = DateTime.Now;
                _context.Entry(itemModel).State = EntityState.Modified;
                await _context.SaveChangesAsync();


                return Ok(itemModel);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // POST: api/ItemModels
        [Route("CreateItem")]
        public async Task<ActionResult<ItemModel>> CreateItem([FromBody] CreateItemModel createItemModel)
        {
            try
            {
                var result = await _context.ItemModels.AddAsync(new ItemModel
                {
                    Id = Guid.NewGuid(),
                    Name = createItemModel.Name,
                    SinglePrice = createItemModel.SinglePrice,
                    UnitNumber = createItemModel.UnitNumber,
                    Discount = createItemModel.Discount,
                    StatusModelGuid = GetStatusGuidByInt(0),
                    Age = createItemModel.Age,
                    Description = createItemModel.Description,
                    PostedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    LocationModelId = Guid.Parse(createItemModel.LocationModelId),
                    CategoryModelId = Guid.Parse(createItemModel.CategoryModelId),
                    ConditionModelId = Guid.Parse(createItemModel.ConditionModelId),
                    TransportModel = createItemModel.TransportModel,
                    UserId = (Guid)HttpContext.Items["UserId"]
                });

                await _context.SaveChangesAsync();
                return Ok(result.Entity);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        //Update status and version
        [Route("SetItemStatus")]
        public async Task<IActionResult> SetItemStatus(Guid id, Guid newStatusGuid)
        {
            try
            {
                var fitemmodel = _context.ItemModels.Find(id);
                if (fitemmodel == null)
                {
                    return NotFound();
                }
                else
                {
                    if (fitemmodel.StatusModelGuid != newStatusGuid)
                    {
                        fitemmodel.UpdatedAt = DateTime.Now;
                        fitemmodel.StatusModelGuid = newStatusGuid;
                        await _context.SaveChangesAsync();
                    }
                    return Ok();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // DELETE: Item and images
        [HttpDelete]
        public async Task<ActionResult<ItemModel>> DeleteItemModel(Guid itemId)
        {
            //Delete the physical files
            var ImageList = _context.ImageModels.Where(x => x.ItemModel.Id == itemId);
            if (ImageList != null)
            {
                foreach (ImageModel imageModel in ImageList)
                {
                    //Delete file
                    System.IO.File.Delete(Path.Combine(_webHostEnvironment.WebRootPath, imageModel.ImagePath));
                }
            }

            //Delete records from the tables, ImageModel table has Delete cascade set in SQL!
            var ItemModel = await _context.ItemModels.FindAsync(itemId);
            if (ItemModel == null)
            {
                return NotFound();
            }

            _context.ItemModels.Remove(ItemModel);
            await _context.SaveChangesAsync();

            return ItemModel;
        }

        // DELETE: Image
        [Route("DeleteImageModel")]
        public async Task<ActionResult<ImageModel>> DeleteImageModel(Guid imageId)
        {
            //Delete records from the table
            var ImageModel = await _context.ImageModels.FindAsync(imageId);
            if (ImageModel == null)
            {
                return NotFound();
            }

            //Delete physical file
            System.IO.File.Delete(Path.Combine(_webHostEnvironment.WebRootPath, ImageModel.ImagePath));

            _context.ImageModels.Remove(ImageModel);
            await _context.SaveChangesAsync();

            if (ImageModel.SequenceId == 1 && _context.ImageModels.Where(x => x.ItemModelId == ImageModel.ItemModelId).Count() > 0)
            {
                _context.ImageModels.Where(x => x.ItemModelId == ImageModel.ItemModelId)
                    .OrderBy(x => x.SequenceId).FirstOrDefault().SequenceId = 1;
                await _context.SaveChangesAsync();
            }

            return ImageModel;
        }

        // SEND MESSAGE
        [Route("SendMessage")]
        public async Task<ActionResult<MessageModel>> SendMessage(Guid buyerId, string isFromBuyer, Guid itemId, string body)
        {
            bool fromBuyer = false;
            if (isFromBuyer == '1'.ToString())
            {
                fromBuyer = true;
            }
            ItemModel itemModel = await _context.ItemModels.FindAsync(itemId);
            User seller = await _context.Users.FindAsync(itemModel.UserId);
            User buyer = await _context.Users.FindAsync(buyerId);
            try
            {
                var result = await _context.MessageModels.AddAsync(new MessageModel
                {
                    Id = Guid.NewGuid(),
                    PostedAt = DateTime.Now,
                    BuyerId = buyerId,
                    IsFromBuyer = fromBuyer,
                    ItemModelId = itemId,
                    Body = body,
                    IsNew = true,
                    IsDeletedByBuyer = false,
                    IsDeletedByItemOwner = false
                });

                await _context.SaveChangesAsync();
                if (fromBuyer) {
                    _emailservice.SendEmailAsync(seller.Email, "Új üzeneted érkezett!", "Üzenetek megtekintése: https://bartermester.com/BackPack/ListMessage");
                } else {
                    _emailservice.SendEmailAsync(buyer.Email, "Új üzeneted érkezett!", "Üzenetek megtekintése: https://bartermester.com/BackPack/ListMessage");
                }
                return Ok(result.Entity);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // GET: api/MessageModels
        [Route("getMessagesbyUser")]
        public async Task<ActionResult<IEnumerable<MessageModel>>> getMessagesbyUser()
        {
            try
            {
                return await _context.MessageModels
                    .Include(x => x.User)
                    .Include(x => x.ItemModel)
                    .ThenInclude(b => b.User)
                    .Where(m => m.BuyerId.ToString() == HttpContext.Items["UserId"].ToString() || m.ItemModel.UserId.ToString() == HttpContext.Items["UserId"].ToString())
                    .OrderByDescending(x => x.IsNew)
                    .ThenByDescending(d => d.PostedAt)
                    .ToListAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        //Update status and version
        [Route("UpdateMessageStatus")]
        public async Task<IActionResult> UpdateMessageStatus(Guid messageId, string isNew)
        {
            bool fisNew = false;
            if (isNew == '1'.ToString())
            {
                fisNew = true;
            }

            try
            {
                var fmessageModel = _context.MessageModels.Find(messageId);
                if (fmessageModel == null)
                {
                    return NotFound();
                }
                else
                {                    
                    fmessageModel.IsNew = fisNew;
                    await _context.SaveChangesAsync();

                    return Ok();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        //Update deleted status
        [Route("UpdateMessageDeletedStatus")]
        public async Task<IActionResult> UpdateMessageDeletedStatus(Guid messageId, string isDeletedByBuyer, string isDeletedByItemOwner)
        {
            bool fisDeletedByBuyer = false;
            if (isDeletedByBuyer == '1'.ToString())
            {
                fisDeletedByBuyer = true;
            }
            bool fisDeletedByItemOwner = false;
            if (isDeletedByItemOwner == '1'.ToString())
            {
                fisDeletedByItemOwner = true;
            }

            try
            {
                var fmessageModel = _context.MessageModels.Find(messageId);
                if (fmessageModel == null)
                {
                    return NotFound();
                }
                else
                {
                    fmessageModel.IsDeletedByBuyer = fisDeletedByBuyer;
                    fmessageModel.IsDeletedByItemOwner = fisDeletedByItemOwner;
                    await _context.SaveChangesAsync();

                    return Ok();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        public Guid GetStatusGuidByInt(int intId)
        {
            return _context.StatusModels.Where(a => a.Id == intId).FirstOrDefault().Guid;
        }

    }
}
