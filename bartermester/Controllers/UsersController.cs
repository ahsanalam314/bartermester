﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BarterMester.HelperCode;
using BarterMester.Models;
using BarterMester.Models.Transaction;
using BarterMester.Models.DemandManagement;
using BarterMester.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using static BarterMester.Services.UserService;
using BarterMester.Models.DBContext;
using BarterMester.Models.UserManagement;

namespace BarterMester.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;
        private IEmailService _emailService;
        private readonly AppSettings _appSettings;
        private readonly BarterMesterDBContext _context;


        public UsersController(IUserService userService, IEmailService emailService, IMapper mapper, IOptions<AppSettings> appSettings, BarterMesterDBContext context)
        {
            _userService = userService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _context = context;
            _emailService = emailService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            var model = _mapper.Map<IList<User>>(users);
            return Ok(model);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(Guid id)
        {
            var user = _userService.GetById(id);
            var model = _mapper.Map<User>(user);
            return Ok(user);
        }

        //Token details
        [Route("getTokenDetails")]
        public async Task<ActionResult<Token>> getTokenDetails(Guid tokenId)
        {
            var token = _context.Tokens.Find(tokenId);

            return Ok(token);
        }

        //Tokens by invitor
        [Authorize(TokenType.LoginToken)]
        [Route("getTokensbyInvitor")]
        public async Task<ActionResult<IEnumerable<Token>>> getTokensbyInvitor()
        {
            try
            {
                return await _context.Tokens
                    .Where(m => m.InvitorId == HttpContext.Items["UserId"].ToString())
                    .OrderByDescending(x => x.CreatedAt)
                    .ToListAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        //Authenticate //  LOGIN
        [HttpPost("Authenticate")]
        public IActionResult Authenticate([FromBody] AuthenticateRequest model)
        {
            var user = _userService.Authenticate(model);

            if (user == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }

            // return basic user info and authentication token
            return Ok(user);
        }


        //inviteUser
        [Authorize(TokenType.LoginToken)]
        [Route("InviteUser")]
        public async Task<ActionResult> InviteUser(string email, Guid userId)
        {

            User user = (User)HttpContext.Items["User"];
            try
            {
                if (user == null)
                {
                    return BadRequest("No User found!");
                }
                await _userService.InviteUser(email, user);
                return Ok();
            }
            catch (AppException e)
            {
                return BadRequest(e);
            }
        }

        //registerviainvitation
        [Route("Register")]
        public async Task<ActionResult> Register([FromBody] RegisterUserRequest model, Guid tokenId)
        {
            Token usedToken = _context.Tokens.Find(tokenId);
            if (usedToken.IsUsed == true)
            {
                return BadRequest("Használt meghívó token!");
            }
            Guid invitorId = Guid.Parse(usedToken.InvitorId);
            AuthenticateResponse createdUser = _userService.Register(model, invitorId);
            try
            {
                usedToken.IsUsed = true;
                usedToken.UsedAt = DateTime.Now;
                await _context.SaveChangesAsync();
                await _context.TransactionFlows.AddAsync(new TransactionFlow
                {
                    Id = new Guid(),
                    Amount = 1500,
                    TransactionType = TransactionType.Register,
                    UserId = createdUser.Id,
                    TransactionStatus = TransactionStatus.Ok,
                    BarterId = Guid.Parse("00000000-0000-0000-0000-000000000000"),
                    TransactionAt = DateTime.Now
                });
                await _context.SaveChangesAsync();
                return Ok(createdUser);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(ex);
            }
        }

        //PasswordResetEmail
        [Route("RequestPasswordChangeEmail")]
        public async Task<IActionResult> RequestPasswordChangeEmail(ResetPasswordEmailRequest model)
        {
            try
            {
                await _userService.SendPasswordChangeEmail(model);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        //Change Password
        [Authorize(TokenType.LoginToken)]
        [Route("ChangePassword")]
        public async Task<IActionResult> ChangePassword(PasswordChangeRequest model)
        {
            try
            {
                User user = (User)HttpContext.Items["User"];
                await _userService.ChangePassword(model, user);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        //Contact Us
        [Authorize(TokenType.LoginToken)]
        [Route("ContactUs")]
        public async Task<ActionResult> ContactUs(string body)
        {
            User user = (User)HttpContext.Items["User"];
            try
            {
                if (user == null)
                {
                    return BadRequest("No User found!");
                }
                _emailService.SendEmailAsync("kapcsolat@bartermester.com", "BarterMester Contact Us", "Feladó: " + user.Email + "Üzenet: " + body);
                return Ok();
            }
            catch (AppException e)
            {
                return BadRequest(e);
            }
        }
    }
}