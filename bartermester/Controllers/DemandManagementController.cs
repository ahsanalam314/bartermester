﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BarterMester.Models;
using BarterMester.Models.ItemManagement;
using Microsoft.AspNetCore.Hosting;
using BarterMester.HelperCode;
using Microsoft.AspNetCore.Http;
using System.IO;
using static BarterMester.Services.UserService;
using BarterMester.Services;
using BarterMester.Models.DBContext;
using BarterMester.Models.DemandManagement;
namespace Bartermester.Controllers
{
    [Authorize(TokenType.LoginToken)]
    [Route("api/[controller]")]
    [ApiController]
    public class DemandManagementController : ControllerBase
    {
        #region defaultControllerConfig
        private readonly BarterMesterDBContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IEmailService _emailservice;

        public DemandManagementController(BarterMesterDBContext context, IWebHostEnvironment webHostEnvironment, IEmailService emailservice)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
            _emailservice = emailservice;
        }
        #endregion

        #region demandManagement
            #region getters
        // GET: api/demandManagement/GetActiveDemands
        [Route("GetActiveDemands")]
        public async Task<ActionResult<IEnumerable<Demand>>> GetActiveDemands()
        {
            try
            {
                return await _context.Demands
                    .Include(x => x.CategoryModel)
                    .Include(s => s.StatusModel)
                    .Include(b => b.User)
                    .Where(m => m.StatusModel.Id == 1)
                    .OrderByDescending(x => x.PostedAt)
                    .ToListAsync();

            }
            //UTÁNAOLVASNI
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        // GET: api/demandManagement/GetDemandsByUser
        [Route("GetDemandsByUser")]
        public async Task<ActionResult<IEnumerable<Demand>>> GetDemandsByUser()
        {
            try
            {
                return await _context.Demands
                    .Include(x => x.CategoryModel)
                    .Include(s => s.StatusModel)
                    .Include(b => b.User)
                    .Where(m => m.UserId == (Guid)HttpContext.Items["UserId"])
                    .OrderBy(d => d.StatusModel.Id)
                    .ThenBy(d => d.Name)
                    .ToListAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        #endregion
            #region setters
        // SET: api/demandManagement/SetDemand
        [Route("SetDemand")]
        public async Task<ActionResult<IEnumerable<Demand>>> SetDemand([FromBody] Demand demand)
        {
            try
            {
                demand.UpdatedAt = DateTime.Now;
                _context.Entry(demand).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return Ok(demand);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // SET: api/demandManagement/SetNewDemand
        [Route("SetNewDemand")]
        public async Task<ActionResult<Demand>> SetNewDemand([FromBody] DemandDto demandDto)
        {
            try
            {
                var result = await _context.Demands.AddAsync(new Demand
                {
                    Id = Guid.NewGuid(),
                    Name = demandDto.Name,
                    Description = demandDto.Description,
                    StatusModelGuid = GetStatusGuidByInt(0),
                    PostedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    CategoryModelId = Guid.Parse(demandDto.CategoryModelId),
                    UserId = (Guid)HttpContext.Items["UserId"]
                });

                await _context.SaveChangesAsync();
                return Ok(result.Entity);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // SET: api/demandManagement/SetDemandStatus
        [Route("SetDemandStatus")]
        public async Task<IActionResult> SetDemandStatus(Guid id, int newStatusId)
        {
            try
            {
                var demand = _context.Demands.Find(id);
                if (demand == null)
                {
                    return NotFound();
                }
                else
                {
                    Guid newStatusGuid = GetStatusGuidByInt(newStatusId);
                    if (demand.StatusModelGuid != newStatusGuid)
                    {
                        demand.UpdatedAt = DateTime.Now;
                        demand.StatusModelGuid = newStatusGuid;
                        await _context.SaveChangesAsync();
                    }
                    return Ok();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        // SET: api/demandManagement/DropDemand
        [Route("DropDemand")]
        public async Task<ActionResult<Demand>> DropDemand(Guid demandId)
        {
            //Delete records from the table
            var demand = await _context.Demands.FindAsync(demandId);
            if (demand == null)
            {
                return NotFound();
            }
            _context.Demands.Remove(demand);
            await _context.SaveChangesAsync();
            return demand;
        }
        #endregion
        #endregion

        #region offerManagement 
            #region getters
        // GET: api/demandManagement/GetOffersbyDemand
        [Route("GetOffersbyDemand")]
        public async Task<ActionResult<List<Offer[]>>> GetOffersbyDemand(Guid demandId)
        {
            try
            {
                var testList = await _context.Offers
                    .Include(q => q.LocationModel)
                    .Include(s => s.OfferStatus)
                    .Include(k => k.OfferImages)
                    .Include(b => b.User)
                    .Include(b => b.Demand)
                    .Where(m => m.Demand.Id == demandId)
                    .OrderByDescending(x => x.PostedAt)
                    .ToListAsync();
                return Ok(testList);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        // GET: api/demandManagement/GetOffersByUser
        [Route("GetOffersByUser")]
        public async Task<ActionResult<IEnumerable<Offer>>> GetOffersByUser()
        {
            try
            {
                return await _context.Offers
                    .Include(q => q.LocationModel)
                    .Include(s => s.OfferStatus)
                    .Include(b => b.User)
                    .Include(b => b.Demand)
                    .Where(m => m.UserId == (Guid)HttpContext.Items["UserId"])
                    .OrderBy(d => d.OfferStatus.Id)
                    .ThenBy(d => d.Name)
                    .ToListAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        [Route("GetImagesbyDemand")]
        public async Task<ActionResult<IEnumerable<OfferImage>>> GetImagesbyDemand(Guid demandId)
        {
            try
            {
                var offers = _context.Offers.Where(x => x.DemandId == demandId).Select(m => m.Id).ToList();
                var offerImgs = await _context.OfferImages.Where(h => offers.Contains(h.OfferId)).Include(w => w.Offer).ToListAsync();
                return Ok(offerImgs);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        #endregion
        #region setters
        // GET: api/demandManagement/SetNewOffer
        [Route("SetNewOffer")]
        public async Task<ActionResult<Offer>> SetNewOffer([FromBody] OfferDto offerDto)
        {
            try
            {
                var offer = await _context.Offers.AddAsync(new Offer
                {
                    Id = Guid.NewGuid(),
                    Name = offerDto.Name,
                    Amount = offerDto.Amount,
                    TransportModel = offerDto.TransportModel,
                    Description = offerDto.Description,
                    OfferStatusGuid = GetOfferStatusGuidByInt(0),
                    LocationModelId = Guid.Parse(offerDto.LocationModelId),
                    DemandId = Guid.Parse(offerDto.DemandId),
                    PostedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    UserId = (Guid)HttpContext.Items["UserId"]
                });

                await _context.SaveChangesAsync();
                return Ok(offer.Entity);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        // SET: api/demandManagement/SetDemandStatus
        [Route("SetOfferStatus")]
        public async Task<IActionResult> SetOfferStatus(Guid id, int newStatusId)
        {
            try
            {
                var offer = _context.Offers.Find(id);
                if (offer == null)
                {
                    return NotFound();
                }
                else
                {
                    Guid newStatusGuid = GetOfferStatusGuidByInt(newStatusId);
                    if (offer.OfferStatusGuid != newStatusGuid)
                    {
                        offer.UpdatedAt = DateTime.Now;
                        offer.OfferStatusGuid = newStatusGuid;
                        await _context.SaveChangesAsync();
                    }
                    return Ok();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        [Route("SetImagesByOffer")]
        public async Task<ActionResult> SetImagesByOffer(Guid offerId)
        {
            try
            {
                //var countExistingFiles = _context.OfferImages.Where(x => x.OfferId == offerId).Count();
                var files = Request.Form.Files;
                var folderName = Path.Combine("StaticFiles/", "Images/");
                var pathToSave = Path.Combine(_webHostEnvironment.WebRootPath, folderName);
                try
                {
                    if (files.Count > 0)
                    {
                        //int i = countExistingFiles + 1;
                        foreach (var file in files)
                        {
                            //var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                            var fileName = Guid.NewGuid();
                            var fullPath = Path.Combine(pathToSave, fileName.ToString() + ".jpg");
                            var dbPath = Path.Combine(folderName, fileName.ToString() + ".jpg");

                            //If the file already exists, an System.IO.IOException exception is thrown
                            using (var stream = new FileStream(fullPath, FileMode.CreateNew))
                            {
                                file.CopyTo(stream);
                            }

                            var result = await _context.OfferImages.AddAsync(new OfferImage
                            {
                                Id = fileName,
                                Name = fileName.ToString() + ".jpg",
                                ImagePath = dbPath,
                                OfferId = offerId,
                                PostedAt = DateTime.Now
                            });

                            await _context.SaveChangesAsync();
                        }
                        return Ok();
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                catch (Exception e)
                {
                    return BadRequest($"Error in write image data to db: {e}");
                }
            }
            catch (Exception e)
            {
                if (e.InnerException.ToString() == "System.IO.IOException")
                {
                    return BadRequest($"Filename already exist: {e}");
                }
                return BadRequest($"Internal server error: {e}");
            }
        }
        // GET: api/demandManagement/AcceptOffer
        [Route("AcceptOffer")]
        public async Task<ActionResult<IEnumerable<Offer>>> AcceptOffer([FromBody] Offer offer)
        {
            try
            {
                var _demand = offer.Demand;
                offer.OfferStatusGuid = GetOfferStatusGuidByInt(3);
                offer.UpdatedAt = DateTime.Now;
                _context.Entry(offer).State = EntityState.Modified;
                _context.SaveChanges();

                var otherOfferList = _demand.Offers;
                foreach (Offer otherOffer in otherOfferList)
                {
                    otherOffer.OfferStatusGuid = GetOfferStatusGuidByInt(2);
                    _context.SaveChanges();
                }
                _demand.StatusModelGuid = GetStatusGuidByInt(3);
                await _context.SaveChangesAsync();

                //send notification emails, messages

                return Ok(offer);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        // GET: api/demandManagement/RejectOffer
        [Route("RejectOffer")]
        public async Task<ActionResult<IEnumerable<Offer>>> RejectOffer([FromBody] Offer offer)
        {
            try
            {
                offer.OfferStatusGuid = GetOfferStatusGuidByInt(2);
                offer.UpdatedAt = DateTime.Now;
                _context.Entry(offer).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                //send notification email

                return Ok(offer);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        // GET: api/demandManagement/CounterOffer
        [Route("CounterOffer")]
        public async Task<ActionResult<IEnumerable<Offer>>> CounterOffer([FromBody] Offer offer)
        {
            try
            {
                offer.OfferStatusGuid = GetOfferStatusGuidByInt(1);
                offer.UpdatedAt = DateTime.Now;
                _context.Entry(offer).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                //send notification email

                return Ok(offer);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        // GET: api/demandManagement/DropOffer
        [HttpDelete]
        public async Task<ActionResult<Offer>> DropOffer(Guid offerId)
        {
            //Delete the physical files
            var imageList = _context.OfferImages.Where(x => x.Offer.Id == offerId);
            if (imageList != null)
            {
                foreach (OfferImage offerImage in imageList)
                {
                    //Delete file
                    System.IO.File.Delete(Path.Combine(_webHostEnvironment.WebRootPath, offerImage.ImagePath));
                }
            }

            //Delete records from the tables, ImageModel table has Delete cascade set in SQL!
            var offer = await _context.Offers.FindAsync(offerId);
            if (offer == null)
            {
                return NotFound();
            }

            _context.Offers.Remove(offer);
            await _context.SaveChangesAsync();

            return offer;
        }

        // GET: api/demandManagement/DropOfferImage
        [Route("DropOfferImage")]
        public async Task<ActionResult<OfferImage>> DropOfferImage(Guid offerImageId)
        {
            //Delete records from the table
            var offerImage = await _context.OfferImages.FindAsync(offerImageId);
            if (offerImage == null)
            {
                return NotFound();
            }

            //Delete physical file
            System.IO.File.Delete(Path.Combine(_webHostEnvironment.WebRootPath, offerImage.ImagePath));

            _context.OfferImages.Remove(offerImage);
            await _context.SaveChangesAsync();

            return offerImage;
        }
        #endregion
        #endregion

        #region internalMethods
        public Guid GetStatusGuidByInt(int intId)
        {
            return _context.StatusModels.Where(a => a.Id == intId).FirstOrDefault().Guid;
        }
        public Guid GetOfferStatusGuidByInt(int intId)
        {
            return _context.OfferStatuses.Where(a => a.Id == intId).FirstOrDefault().Guid;
        }
        #endregion
    }
}