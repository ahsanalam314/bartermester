import { Injectable, Inject } from '@angular/core';
import { ItemModel, CreateItemModel, ImageModel, MessageModel } from '../_models/item-management.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { SpinnerService } from '../_services/spinner.service';
import { BarterManagementService } from './barter-management.service';
import { map } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';
import { AuthenticationService } from './authentication.service';
import { User } from '../_models/user';
import { Demand, DemandDto, OfferDto, Offer, OfferImage } from '../_models/demand-management.model';

@Injectable({
  providedIn: 'root'
})
export class DemandManagementService {
  recordlistOwnedDemands$: BehaviorSubject<Demand[]> = new BehaviorSubject([]);
  recordlistActiveDemands$: BehaviorSubject<Demand[]> = new BehaviorSubject([]);
  recordlistOffers$: BehaviorSubject<Offer[]> = new BehaviorSubject([]);
  recordlistImgs$: BehaviorSubject<OfferImage[]> = new BehaviorSubject([]);
  formData: FormData = new FormData();
  constructor(
    private http: HttpClient,
    @Inject('APIUrl') private APIUrl: string,
    private SpinnerService: SpinnerService,
    private authenticationService: AuthenticationService,
  ) { }

  //getters
    getActiveDemands() {
      this.http.get<Demand[]>(`${this.APIUrl}/api/demandManagement/getActiveDemands`).subscribe(res => {
        this.recordlistActiveDemands$.next([...res]);
      });
      return this.recordlistActiveDemands$;
    }
    getDemandsByUser() {
      this.http.get<Demand[]>(`${this.APIUrl}/api/demandManagement/getDemandsByUser`).subscribe(data => {
        this.recordlistOwnedDemands$.next([...data]);
      });
      return this.recordlistOwnedDemands$;
  }
    getImagesByDemand(demandId: string) {
      return this.http.get<OfferImage[]>(`${this.APIUrl}/api/demandManagement/getImagesByDemand`, {
        params: new HttpParams().set('demandId', demandId)
      });
    }
    getOffersByDemand(currentDemandId: string) {
      this.http.get<Offer[]>(`${this.APIUrl}/api/demandManagement/getOffersByDemand`, {
        params: new HttpParams().set('demandId', currentDemandId)
      }).subscribe(data => {
        this.recordlistOffers$.next([...data]);
      });
      return this.recordlistOffers$;
    }
  //setters
    setDemand(record: Demand) {
      return this.http.post<Demand>(`${this.APIUrl}/api/demandManagement/setDemand`, record);
    }
    setDemandStatus(newStatus: number, currentDemandId: string) {
      return this.http.get<Demand>(`${this.APIUrl}/api/demandManagement/setDemandStatus`, {
        params: new HttpParams()
          .set('id', currentDemandId)
          .set('newStatusId', newStatus.toString())
      });
  }
  setOfferStatus(newStatus: number, currentOfferId: string) {
    return this.http.get<Offer>(`${this.APIUrl}/api/demandManagement/setOfferStatus`, {
      params: new HttpParams()
        .set('id', currentOfferId)
        .set('newStatusId', newStatus.toString())
    });
  }
    setNewDemand(record: DemandDto) {
      return this.http.post<Demand>(`${this.APIUrl}/api/demandManagement/setNewDemand`, record);
    }
    setNewOffer(record: OfferDto) {
      return this.http.post<Offer>(`${this.APIUrl}/api/demandManagement/setNewOffer`, record);
    }
    deleteDemand(demandId: string) {
      return this.http.delete(`${this.APIUrl}/api/demandManagement/dropDemand`, {
        params: new HttpParams()
          .set('demandId', demandId)
      });
    }
    setImagesbyOffer(offerId: string, files: File[]) {
      this.SpinnerService.show();
      this.formData = new FormData();
      files.forEach(j => this.formData.append('file', j));
      return this.http.post(`${this.APIUrl}/api/demandManagement/setImagesByOffer`, this.formData, {
        headers: new HttpHeaders()
          .set('Accept', 'application/json')
          .set('Response-Type', 'json'),
        params: new HttpParams()
          .set('offerId', offerId)
      }).subscribe(res => {
        this.SpinnerService.hide();
      });
    }
}

