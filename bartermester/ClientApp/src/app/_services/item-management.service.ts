import { Injectable, Inject } from '@angular/core';
import { ItemModel, CreateItemModel, ImageModel, MessageModel } from '../_models/item-management.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { SpinnerService } from '../_services/spinner.service';
import { BarterManagementService } from './barter-management.service';
import { map } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';
import { AuthenticationService } from './authentication.service';
import { User } from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class ItemManagementService {
  recordlistBackpack$: BehaviorSubject<ItemModel[]> = new BehaviorSubject([]);
  recordlistMarketplace$: BehaviorSubject<ItemModel[]> = new BehaviorSubject([]);
  recordlistImage$: BehaviorSubject<ImageModel[]> = new BehaviorSubject([]);
  recordlistMessage$: BehaviorSubject<MessageModel[]> = new BehaviorSubject([]);
  formData: FormData = new FormData();
  newMsgCount$: BehaviorSubject<number> = new BehaviorSubject(0);
  value = '';
  constructor(
    private http: HttpClient,
    @Inject('APIUrl') private APIUrl: string,
    private SpinnerService: SpinnerService,
    private barterManagementService: BarterManagementService,
    private authenticationService: AuthenticationService,
  ) {
    //this.http.get<ItemModel[]>(`${this.APIUrl}/api/itemmodels/GetAllActiveItem`).subscribe(result => { this.recordlistMarketplace$.next(result); });
  }

  sendInMail(body: string, users: User[]) {
    return this.http.post<User[]>(`${this.APIUrl}/api/shared/InMailAdmin`, users, {
      params: new HttpParams().set('body', body)
    })
  }

  sendEmail(body: string, users: User[]) {
    return this.http.post<User[]>(`${this.APIUrl}/api/shared/EmailAdmin`, users, {
      params: new HttpParams().set('body', body)
    })
  }

  getImagesbyItem(currentItemId: string) {
    return this.http.get<ImageModel[]>(`${this.APIUrl}/api/imagemodels/GetImagesbyItem`, {
      params: new HttpParams()
        .set('itemId', currentItemId)
    })
  }

  getImagesbyUser(): Observable<ImageModel[]> {
    return this.recordlistImage$;
  }

  getActiveItem(): Observable<ItemModel[]> {
    this.barterManagementService.setCurrentTotalSum();
    return this.http.get<ItemModel[]>(`${this.APIUrl}/api/itemmodels/GetAllActiveItem`);
  }

  getBackpackItem(): Observable<ItemModel[]> {
    return this.recordlistBackpack$;
  }

  getMessagesbyUser(): Observable<MessageModel[]> {
    return this.recordlistMessage$;
  }

  getNewMessageCount(): Observable<number> {
    return this.newMsgCount$;
  }

  setBackpackItem() {
    this.SpinnerService.show();
    this.http.get<ItemModel[]>(`${this.APIUrl}/api/itemmodels/GetItemByUser`)
      .subscribe(result => {
        this.recordlistBackpack$.next(result);
        this.setImagesbyUser();
        //this.SpinnerService.hide(); //Hidden in  this.setImagesbyUser()
      });
  }

  setImagesbyUser() {
    //this.SpinnerService.show();
    this.http.get<ImageModel[]>(`${this.APIUrl}/api/imagemodels/GetImagesbyUser`)
      .subscribe(result => {
        this.recordlistImage$.next(result);
        this.SpinnerService.hide();
      });
  }

  createItem(record: CreateItemModel) {
    return this.http.post<ItemModel>(`${this.APIUrl}/api/itemmodels/CreateItem`, record);
  }

  createImagebyItem(itemId: string, files: File[]) {
    this.SpinnerService.show();
    this.formData = new FormData();
    files.forEach(j => this.formData.append('file', j));
    return this.http.post(`${this.APIUrl}/api/imagemodels/UploadPicture`, this.formData, {
      headers: new HttpHeaders()
        .set('Accept', 'application/json')
        .set('Response-Type', 'json'),
      params: new HttpParams()
        .set('itemId', itemId)
    }).subscribe(res => {
      this.setImagesbyUser();
      this.SpinnerService.hide();
    });
  }

  modifyItem(record: ItemModel) {
    return this.http.put<ItemModel>(`${this.APIUrl}/api/itemmodels/ModifyItem`, record, {
    });
  }

  setItemStatus(newStatus: string, currentItemId: string) {
    return this.http.get<ItemModel>(`${this.APIUrl}/api/itemmodels/SetItemStatus`, {
      params: new HttpParams()
        .set('id', currentItemId)
        .set('newStatusGuid', newStatus)
    });
  }

  deleteItem(itemId: string) {
    return this.http.delete(`${this.APIUrl}/api/itemmodels`, {
      params: new HttpParams()
        .set('itemId', itemId)
    });
  }

  deleteImageModel(imageId: string) {
    return this.http.get<ImageModel>(`${this.APIUrl}/api/itemmodels/DeleteImageModel`, {
      params: new HttpParams()
        .set('imageId', imageId)
    });
  }

  sendMessage(buyerId: string, isFromBuyer: string, itemId: string, body: string) {
    return this.http.get<MessageModel>(`${this.APIUrl}/api/itemmodels/SendMessage`, {
      params: new HttpParams()
        .set('buyerId', buyerId)
        .set('isFromBuyer', isFromBuyer)
        .set('itemId', itemId)
        .set('body', body)
    });
  }

  setMessagesbyUser() {
    this.SpinnerService.show();
    this.http.get<MessageModel[]>(`${this.APIUrl}/api/itemmodels/getMessagesbyUser`)
      .subscribe(result => {
        this.recordlistMessage$.next(result);

        this.recordlistMessage$.pipe(map(data => data.filter(f =>
          (f.isNew == true && f.isFromBuyer == true && f.buyerId != this.authenticationService.currentUserValue.id && f.isDeletedByItemOwner == false)
          || (f.isNew == true && f.isFromBuyer == false && f.buyerId == this.authenticationService.currentUserValue.id && f.isDeletedByBuyer == false)
        ))).subscribe(res => {
          if (!isNullOrUndefined(res)) {
            this.newMsgCount$.next(res.length);
          }
        });

        this.SpinnerService.hide();
      });
  }

  updateMessageStatus(messageId: string, isNew: string) {
    return this.http.get<MessageModel>(`${this.APIUrl}/api/itemmodels/UpdateMessageStatus`, {
      params: new HttpParams()
        .set('messageId', messageId)
        .set('isNew', isNew)
    });
  }

  updateMessageDeletedStatus(messageId: string, isDeletedByBuyer: string, isDeletedByItemOwner: string) {
    return this.http.get<MessageModel>(`${this.APIUrl}/api/itemmodels/UpdateMessageDeletedStatus`, {
      params: new HttpParams()
        .set('messageId', messageId)
        .set('isDeletedByBuyer', isDeletedByBuyer)
        .set('isDeletedByItemOwner', isDeletedByItemOwner)
    });
  }
}
