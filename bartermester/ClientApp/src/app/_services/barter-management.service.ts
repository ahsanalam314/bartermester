import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Barter, TransactionFlow, CreateBarter } from '../_models/barter-management.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { SpinnerService } from '../_services/spinner.service';

@Injectable({
  providedIn: 'root'
})
export class BarterManagementService {
  transactionFlowList$: BehaviorSubject<TransactionFlow[]> = new BehaviorSubject([]);
  currentTotalSum$: BehaviorSubject<number> = new BehaviorSubject(0);

  constructor(
    private http: HttpClient,
    @Inject('APIUrl') private APIUrl: string,
    private SpinnerService: SpinnerService,
  ) {
   }

  actionBuy(createBarter: CreateBarter) {
    return this.http.post<Barter>(`${this.APIUrl}/api/bartermodels/CreateBarter`, createBarter, {
      params: new HttpParams()
        .set('newStatusId', '3')
    });
  }

  getBalance(): Observable<TransactionFlow[]> {
    this.SpinnerService.show();
    this.http.get<TransactionFlow[]>(`${this.APIUrl}/api/bartermodels/GetBalance`).subscribe(result => {
      this.transactionFlowList$.next(result);
      this.SpinnerService.hide();
    });
    return this.transactionFlowList$;
  }

  setCurrentTotalSum() {
    this.getBalance().subscribe(res => {
      var totalSum = res.reduce((sum, b) => sum + b.amount, 0);
      this.currentTotalSum$.next(totalSum);
    });
  }

  getCurrentTotalSum(): Observable<number> {
    return this.currentTotalSum$.asObservable();
  }


}
