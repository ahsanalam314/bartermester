import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User, UserRegistrationModel, Token, ResetPasswordEmailRequest, PasswordChangeRequest } from '../_models/user';
import { ok } from 'assert';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  public currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  //recordlistUser$: BehaviorSubject<User>;

  constructor(private http: HttpClient, @Inject('APIUrl') private APIUrl: string) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();

  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  getUser(): Observable<User> {
    return this.currentUserSubject;
  }

  getAll() {
    return this.http.get<User[]>(`${this.APIUrl}/api/users`);
  }

  login(username: string, password: string) {
    return this.http.post<User>(`${this.APIUrl}/api/users/authenticate`, { username, password })
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        //this.recordlistUser$.next(user);
        return user;
      }));
  }

  register(data: UserRegistrationModel, tokenId: string) {
    return this.http.post<User>(`${this.APIUrl}/api/users/register`, data, {
      params: new HttpParams()
        .set('tokenId', tokenId)
    })
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
      }));
  }

  changePassword(data: PasswordChangeRequest) {
    return this.http.post(`${this.APIUrl}/api/users/ChangePassword`, data);
  }

  requestPasswordChangeEmail(data: ResetPasswordEmailRequest) {
    return this.http.post(`${this.APIUrl}/api/users/RequestPasswordChangeEmail`, data);
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    //this.recordlistUser$.next(null);
  }

  invite(email: string) {
    return this.http.get(`${this.APIUrl}/api/users/InviteUser`, {
      params: new HttpParams()
        .set('email', email)
    });
  }

  getTokenDetails(tokenId: string) {
    return this.http.get<Token>(`${this.APIUrl}/api/users/getTokenDetails`, {
      params: new HttpParams()
        .set('tokenId', tokenId)
    });
  }

  getTokensbyUser() {
    return this.http.get<Token[]>(`${this.APIUrl}/api/users/getTokensbyInvitor`);
  }

  contactUs(body: string) {
    return this.http.get(`${this.APIUrl}/api/users/ContactUs`, {
      params: new HttpParams()
        .set('body', body)
    });
  }
}
