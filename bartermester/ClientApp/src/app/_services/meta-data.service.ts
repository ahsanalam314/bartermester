import { Injectable, Inject } from '@angular/core';
import { CategoryModel, ConditionModel, LocationModel, StatusModel } from '../_models/meta-data.model';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MetaDataService {
  categorylist$: BehaviorSubject<CategoryModel[]> = new BehaviorSubject([]);
  conditionlist$: BehaviorSubject<ConditionModel[]> = new BehaviorSubject([]);
  locationlist$: BehaviorSubject<LocationModel[]> = new BehaviorSubject([]);
  statuslist$: BehaviorSubject<StatusModel[]> = new BehaviorSubject([]);
  footerList$: BehaviorSubject<StatusModel[]> = new BehaviorSubject([]);

  constructor(private http: HttpClient, @Inject('APIUrl') private APIUrl: string) {
    this.http.get<CategoryModel[]>(`${this.APIUrl}/api/categorymodels/GetAllCategory`).subscribe(result => { this.categorylist$.next(result); });
    this.http.get<ConditionModel[]>(`${this.APIUrl}/api/conditionmodels/GetAllCondition`).subscribe(result => { this.conditionlist$.next(result); });
    this.http.get<LocationModel[]>(`${this.APIUrl}/api/locationmodels/GetAllLocation`).subscribe(result => { this.locationlist$.next(result); });
    this.http.get<StatusModel[]>(`${this.APIUrl}/api/statusmodels/GetAllStatus`).subscribe(result => { this.statuslist$.next(result); });
    this.http.get<any[]>(`${this.APIUrl}/api/footermodels/GetAllFooters`).subscribe(result => { this.footerList$.next(result); });
  }

  getAllCategory(): Observable<CategoryModel[]> { return this.categorylist$; }

  getAllCondition(): Observable<ConditionModel[]> { return this.conditionlist$; }

  getAllLocation(): Observable<LocationModel[]> { return this.locationlist$; }

  getAllStatus(): Observable<StatusModel[]> { return this.statuslist$; }

  getAllFooters(): Observable<any[]> { return this.footerList$; }
}
