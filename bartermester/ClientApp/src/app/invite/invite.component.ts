import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../_services/authentication.service';
import { SpinnerService } from '../_services/spinner.service';
import { Token } from '../_models/user';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.css']
})
export class InviteComponent implements OnInit {
  inviteForm: FormGroup;
  inviteHistory: Array<Token> = [];
  loading = false;

  constructor(
    private formBuilder: FormBuilder,
    private SpinnerService: SpinnerService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {  }

  ngOnInit() {
    this.inviteForm = this.formBuilder.group({
      email: ['', Validators.required]
    });
    this.loading = true;
    this.authenticationService.getTokensbyUser().subscribe(res => {
      this.inviteHistory = res;
      this.loading = false;
    });
  }

  onSubmit() {
    if (this.inviteForm.invalid) {
      alert("Írj be egy helyes e-mail címet!");
    }
    else {
      if (this.inviteHistory.filter(x => x.email == this.inviteForm.controls['email'].value).length >= 1) {
        alert("Őt már korábban meghívtad.");
        return;
      }
      this.SpinnerService.show();
      this.authenticationService.invite(this.inviteForm.controls['email'].value).subscribe(res => {
        this.SpinnerService.hide();
        this.router.navigate(['']);
      });
    }   
  }

  onCancel() {
    this.router.navigate(['']);
  }

}
