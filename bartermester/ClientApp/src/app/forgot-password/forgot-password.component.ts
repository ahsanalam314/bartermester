import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../_services/authentication.service';
import { SpinnerService } from '../_services/spinner.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  hide = true;
  hide2 = true;
  forgotPasswordForm: FormGroup;
  loading = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private SpinnerService: SpinnerService,
  ) {
  }

  ngOnInit() {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', Validators.required]      
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.forgotPasswordForm.controls; }

  onSubmit() {
    // stop here if form is invalid
    if (this.forgotPasswordForm.invalid) {
      alert("Add meg az email címed!")
      return;
    }

    this.SpinnerService.show();
    //this.authenticationService.requestPasswordChangeEmail({ email: "peter.bereznay@bots2work.com" }).subscribe();
    this.authenticationService.requestPasswordChangeEmail({ email: this.f.email.value })
      .pipe(first())
      .subscribe(
        data => {
          alert("Emailben küldtünk neked egy új jelszót.")
          this.SpinnerService.hide();
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.error = error;
          alert("Ez most nem sikerült.")
          this.SpinnerService.hide();
        });
  }

  onCancel() {
    this.router.navigate([this.returnUrl]);
  }
}
