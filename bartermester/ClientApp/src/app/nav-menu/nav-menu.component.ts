import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/authentication.service';
import { SpinnerService } from '../_services/spinner.service';
import { BarterManagementService } from '../_services/barter-management.service';
import { MatDialog } from '@angular/material';
import { ViewBalanceComponent } from '../view-balance/view-balance.component';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { ItemManagementService } from '../_services/item-management.service';
import { FooterManagerComponent } from './footer-manager/footer-manager.component';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  isCurrentUser: boolean;
  userName: Observable<string>;
  title = 'app';
  isExpanded = false;
  totalSum = 0;
  msgCount = 0;
  isAdmin = false;
  isDarkTheme: boolean = false;
  isChikani: boolean = false;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private SpinnerService: SpinnerService,
    private barterManagementService: BarterManagementService,
    private itemManagementService: ItemManagementService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit() {
    this.userName = this.authenticationService.currentUser.pipe(filter(j => j != null), map(j => j.username));

    this.userName.pipe(filter(j => j !== null)).subscribe(result => {
      this.barterManagementService.setCurrentTotalSum();
      this.barterManagementService.getCurrentTotalSum().subscribe(res => this.totalSum = res);
      this.itemManagementService.setMessagesbyUser();
      this.itemManagementService.getNewMessageCount().subscribe(res => this.msgCount = res);
      this.isCurrentUser = true;
    },
      error => {
        console.log("No user at nav-menu oninit");
        this.SpinnerService.hide();
      },
      () => console.log("navbar valid user subscription ends")
    )
    this.authenticationService.getUser().subscribe(res => {
      if (res.username == "BartermesterAdmin" || res.username == "peterb90" || res.username == "peterb1990" || res.username == "BoomBoomBetty" || res.username == "chikani") {
        this.isAdmin = true;
      }

      if (res.username == "chikani") {
        this.isChikani = true;
      }
    });
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
    this.isCurrentUser = false;
  }

  login() {
    this.router.navigate(['/login']);
  }

  onSumDetailsClick() {
    let dialogRef = this.dialog.open(ViewBalanceComponent, {})
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  changePassword() {
    this.router.navigate(['/changepassword']);
  }

  themeToggleChange() {
    console.log('toggle');
    this.SpinnerService.show();
    if (this.isDarkTheme) {
      let head = document.head || document.getElementsByTagName('head')[0];
      let link: HTMLLinkElement = document.createElement('link');
      link.href = 'assets/css/dark-theme.css';
      link.rel = 'stylesheet';
      link.id = 'dark-theme-link';
      head.appendChild(link);
    } else {
      let linkDarkTheme = document.getElementById('dark-theme-link');
      if (linkDarkTheme) {
        linkDarkTheme.remove();
      }
    }
    setTimeout(() => this.SpinnerService.hide(), 1000);
  }

  openFooterPopup() {
    this.dialog.open(FooterManagerComponent, { height: '50vh', width: '50%' });
  }

}
