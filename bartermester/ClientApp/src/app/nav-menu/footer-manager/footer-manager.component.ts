import { Component, OnInit } from '@angular/core';
import { MetaDataService } from 'src/app/_services/meta-data.service';

@Component({
  selector: 'app-footer-manager',
  templateUrl: './footer-manager.component.html',
  styleUrls: ['./footer-manager.component.css']
})
export class FooterManagerComponent implements OnInit {

  footers: any[] =[];
  selectedFooter: any;
  footerText: string;

  constructor(private _mds: MetaDataService) { }

  ngOnInit() {
    // this.getFooters();
  }

  getFooters() {
    this._mds.getAllFooters().subscribe(f => this.footers = f);
  }

  updateFooter() {
    if (this.selectedFooter && this.footerText) {

    }
  }

}
