import { Component, OnInit } from '@angular/core';
import { ItemManagementService } from '../../_services/item-management.service';
import { ItemModel } from '../../_models/item-management.model';
import { MatDialog } from '@angular/material';
import { ViewItemComponent } from '../view-item/view-item.component';
import { isNullOrUndefined } from 'util';
import { SpinnerService } from '../../_services/spinner.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'app-market-place',
  templateUrl: './market-place.component.html',
  styleUrls: ['./market-place.component.css']
})
export class MarketPlaceComponent implements OnInit {
  returnUrl: string;
  tiles: Array<ItemModel> = [];
  myResult: Array<ItemModel> = [];
  locationList: Array<ItemModel> = [];
  conditionList: Array<ItemModel> = [];
  categoryList: Array<ItemModel> = [];
  minPrice = 0;
  maxPrice = 0;
  locationModelId = '';
  conditionModelId = '';
  categoryModelId = '';
  locationReset = '';
  categoryReset = '';
  minPriceReset = '';
  maxPriceReset = '';
  conditionReset = '';
  randomNum = 0;
  numoftiles: any;
  numofpseudo: any;
  x = 6;
  y = 0; 
  isAdmin = false;
  constructor(
    private ItemManagementService: ItemManagementService,
    private SpinnerService: SpinnerService,
    public dialog: MatDialog,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,

    private authenticationService: AuthenticationService,
  ) {
    
  }

  ngOnInit() {
    this.onRefreshClick();
    this.authenticationService.getUser().subscribe(res => {
      if (res.username == "BartermesterAdmin" || res.username == "peterb90" || res.username == "peterb1990" || res.username == "BoomBoomBetty" || res.username == "chikani") {
        this.isAdmin = true;
      }
    });
  }

  refreshMarketplace() {
    this.ItemManagementService.getActiveItem().subscribe(res => {
      if (!isNullOrUndefined(res)) {
        this.tiles = res;
        this.numoftiles = this.tiles.length;
        this.numofpseudo = Math.round(this.numoftiles / 4);
        this.x = 6;
        this.y = 0;
        if (this.numoftiles == 0) { }
        else {
          for (var i = 0; i < this.numofpseudo; i++) {
            this.tiles.splice(Math.floor(Math.random() * (this.x - this.y) + this.y), 0,
              {
                id: 'pseudo', name: "pseudo", singlePrice: 0, unitNumber: 0, discount: 0, statusModel: this.tiles[0].statusModel,
                statusModelGuid: "", age: "", description: "", postedAt: new Date(), updatedAt: new Date(), locationModel: this.tiles[0].locationModel,
                locationModelId: "", conditionModel: this.tiles[0].conditionModel, conditionModelId: "", categoryModel: this.tiles[0].categoryModel,
                categoryModelId: "", userId: "", transportModel: "", user: this.tiles[0].user, imageUrls: ""
              });
            this.x = this.x + 5;
            this.y = this.y + 5;
          }

          this.myResult = this.tiles;

          this.locationList = this.tiles.filter((thing, index, self) =>
            self.findIndex(t => t.locationModelId === thing.locationModel.id) === index)
            .sort((a, b) => a.locationModel.city.localeCompare(b.locationModel.city));

          this.conditionList = this.tiles.filter((thing, index, self) =>
            self.findIndex(t => t.conditionModelId === thing.conditionModel.id) === index)
            .sort((a, b) => a.conditionModel.name.localeCompare(b.conditionModel.name));

          this.categoryList = this.tiles.filter((thing, index, self) =>
            self.findIndex(t => t.categoryModelId === thing.categoryModel.id) === index)
            .sort((a, b) => a.categoryModel.name.localeCompare(b.categoryModel.name));          
        }
      }
    });    
  }

  onRefreshClick() {
    this.SpinnerService.show(); 
    this.refreshMarketplace();
    this.ItemManagementService.setMessagesbyUser();

    //Reset filters
    this.minPrice = 0;
    this.maxPrice = 0;
    this.locationModelId = '';
    this.conditionModelId = '';
    this.categoryModelId = '';

    this.locationReset = '';
    this.categoryReset = '';
    this.minPriceReset = '';
    this.maxPriceReset = '';
    this.conditionReset = '';
  }

  openDialog(obj) {
    const dialogRef = this.dialog.open(ViewItemComponent, {
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        //this.onRefreshClick();
      }
    });
  }

  setClasses(myCategory: string) {
    //piros:#E4112A; - Divat, ruházat
    //narancs:#FFBF00; - Otthon, háztartás
    //zold:#80b918; - Szabadidő, sport
    //kek:#1183D0; -  Műszaki cikkek, elektronika
    //lila:#9b5de5; - Kultúra
    //világoskék:#56C7F0; - Baba - mama

    let myClasses = {
      hex_red: myCategory === 'Divat, ruházat',
      hex_orange: myCategory === 'Otthon, háztartás',
      hex_green: myCategory === 'Szabadidő, sport',
      hex_blue: myCategory === 'Műszaki cikkek, elektronika',
      hex_purple: myCategory === 'Kultúra',
      hex_lightblue: myCategory === 'Baba-mama'
    };
    return myClasses;
  }

  onLocationSelected(event) {
    this.locationModelId = event.value;
  }

  onCategorySelected(event) {
    this.categoryModelId = event.value;
  }

  onMinPriceSelected($event) {
    if ($event.target.value >= 0) {
      this.minPrice = $event.target.value;
    } else {
      alert('A minimum ár csak pozitív szám lehet!')
    }
  }

  onMaxPriceSelected($event) {
    if ($event.target.value >= 0) {
      this.maxPrice = $event.target.value;
    } else {
      alert('A maximum ár csak pozitív szám lehet!')
    }
  }

  onConditionSelected(event) {
    this.conditionModelId = event.value;
  }

  runFilter() {
    this.tiles = this.myResult;

    if (this.locationModelId != '') {
      this.tiles = this.tiles.filter(x => x.locationModelId == this.locationModelId);
    }
    if (this.categoryModelId != '') {
      this.tiles = this.tiles.filter(x => x.categoryModelId == this.categoryModelId);
    }
    if (this.minPrice != 0) {
      this.tiles = this.tiles.filter(x => x.singlePrice >= this.minPrice);
    }
    if (this.maxPrice != 0) {
      this.tiles = this.tiles.filter(x => x.singlePrice <= this.maxPrice);
    }
    if (this.conditionModelId != '') {
      this.tiles = this.tiles.filter(x => x.conditionModelId == this.conditionModelId);
    }

    //this.locationReset = '';
    //this.categoryReset = '';
    //this.minPriceReset = '';
    //this.maxPriceReset = '';
    //this.conditionReset = '';
  }
  onNavigateToItems() {
    this._router.navigate(['/invite']);
    alert(this._activatedRoute.root.toString());
  }
  onNavigateToDemands() {

  }
}
