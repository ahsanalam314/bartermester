import { Component, Optional, Inject, OnInit } from '@angular/core';
import { ItemModel } from '../../_models/item-management.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SpinnerService } from '../../_services/spinner.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { ItemManagementService } from '../../_services/item-management.service';
import { invalid } from '@angular/compiler/src/render3/view/util';

@Component({
  selector: 'app-send-message',
  templateUrl: './send-message.component.html',
  styleUrls: ['./send-message.component.css']
})
export class SendMessageComponent implements OnInit {
  popUpForm: FormGroup;

  constructor
    (
      private SpinnerService: SpinnerService,
      private dialogRef: MatDialogRef<SendMessageComponent>,
      private formBuilder: FormBuilder,
      private itemManagementService: ItemManagementService,
      private authenticationService: AuthenticationService,
      @Optional() @Inject(MAT_DIALOG_DATA) public data: ItemModel,
      @Inject('APIUrl') private APIUrl: string
    ) {
  }

  ngOnInit() {
    this.popUpForm = this.formBuilder.group({
      body: ['', Validators.required]
    });
  }

  onSubmit(itemModel: ItemModel) {
    if (this.popUpForm.invalid) {
      return;
    }

    if (itemModel.userId == this.authenticationService.currentUserValue.id) {
      alert("A terméket Te hírdeted!")
    }

    else {
      this.SpinnerService.show();
      this.itemManagementService.sendMessage(this.authenticationService.currentUserValue.id, '1', itemModel.id, this.popUpForm.controls['body'].value).subscribe(res => {
        if (res != undefined) {
          alert("Üzenet elküldve.");
          this.dialogRef.close(res);
        } else {
          alert("Ez most nem sikerült.");
        }
        this.SpinnerService.hide();
      });
    }
  }
}
