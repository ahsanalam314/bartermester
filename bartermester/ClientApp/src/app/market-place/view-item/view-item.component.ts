import { Component, Optional, Inject, OnInit } from '@angular/core';
import { ItemModel, ImageModel } from '../../_models/item-management.model';
import { FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { BarterManagementService } from '../../_services/barter-management.service';
import { SpinnerService } from '../../_services/spinner.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { ItemManagementService } from '../../_services/item-management.service';
import { isNullOrUndefined } from 'util';
import { SendMessageComponent } from '../send-message/send-message.component';
import { ConfirmationDialogComponent } from '../../confirmation-dialog/confirmation-dialog.component';


@Component({
  selector: 'app-view-item',
  templateUrl: './view-item.component.html',
  styleUrls: ['./view-item.component.css']
})
export class ViewItemComponent implements OnInit {
  action: string;
  local_data: any;
  popUpForm: FormGroup;
  private totalSum = 0;
  imageFiles: Array<ImageModel> = [];

  constructor
    (
      private SpinnerService: SpinnerService,
      private dialogRef: MatDialogRef<ViewItemComponent>,
      private itemManagementService: ItemManagementService,
      private barterManagementService: BarterManagementService,
      private authenticationService: AuthenticationService,
      public dialog: MatDialog,
      @Optional() @Inject(MAT_DIALOG_DATA) public data: ItemModel,
      @Inject('APIUrl') private APIUrl: string

    ) {
  }

  ngOnInit() {
    this.local_data = { ...this.data };
    this.action = this.local_data.action;
    this.getImages();
  }

  getImages() {
    this.SpinnerService.show();
    this.itemManagementService.getImagesbyItem(this.data.id).subscribe(res => {
      if (!isNullOrUndefined(res)) {
        this.imageFiles = res;
      }
      //else {  }
      this.SpinnerService.hide();
    });
  }

  onSubmit(itemModel: ItemModel) {
    this.barterManagementService.getCurrentTotalSum().subscribe(res => this.totalSum = res + (1 / 100));

    if (itemModel.userId == this.authenticationService.currentUserValue.id) {
      alert("A terméket nem veheted meg, mert Te hírdeted!")
    }
    else if (itemModel.singlePrice >= this.totalSum) {
      alert("Nincs elég Bartek a tulajdonodban.")
    }
    else {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent);
      dialogRef.afterClosed().subscribe(result => {
        if (result == "confirmed") {
          this.SpinnerService.show();
          var tempCreateBarter = {
            itemModelId: itemModel.id,
            comment: 'Test',
            location: 'Test Location'
          }
          this.barterManagementService.actionBuy(tempCreateBarter).subscribe(res => {
            if (res != undefined) {
              this.barterManagementService.setCurrentTotalSum();
              this.barterManagementService.getBalance();
              alert("Ezt megvetted!");
              this.dialogRef.close(res);
            } else {
              alert("Ez most nem sikerült.");
            }
            this.SpinnerService.hide();
          });
        }
      });
    }
  }

  onSendMessage(itemModel: ItemModel) {
    const dialogRef = this.dialog.open(SendMessageComponent, {
      data: itemModel
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        //alert("Üzenet elküldve.");
      }
    });
  }
}
