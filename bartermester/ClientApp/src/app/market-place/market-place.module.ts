import { NgModule, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketPlaceComponent } from './market-place/market-place.component';
import { MarketPlaceRoutingModule } from './market-place-routing.module';
import { MaterialModule } from '../material.module';
import { ViewItemComponent } from './view-item/view-item.component';
import { SendMessageComponent } from './send-message/send-message.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgcCookieConsentModule } from 'ngx-cookieconsent';
@NgModule({
  declarations: [ViewItemComponent, MarketPlaceComponent, SendMessageComponent],
  entryComponents: [ViewItemComponent, SendMessageComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MarketPlaceRoutingModule,
    MaterialModule,
    NgcCookieConsentModule
  ]
})
export class MarketPlaceModule { }
