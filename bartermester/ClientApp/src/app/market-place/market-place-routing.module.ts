import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketPlaceComponent } from './market-place/market-place.component';
import { Routes, RouterModule } from '@angular/router';
import { ViewItemComponent } from './view-item/view-item.component';
import { SendMessageComponent } from './send-message/send-message.component';
import { ColorSelectorComponent } from './color-selector/color-selector.component';

const routes: Routes = [
  { path: 'MarketPlace', component: MarketPlaceComponent },
  { path: 'ViewItem', component: ViewItemComponent },
  { path: 'SendMessage', component: SendMessageComponent },
  { path: 'color-selector', component: ColorSelectorComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarketPlaceRoutingModule { }

