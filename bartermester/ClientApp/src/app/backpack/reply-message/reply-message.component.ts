import { Component, Optional, Inject, OnInit } from '@angular/core';
import { ItemModel, MessageModel } from '../../_models/item-management.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SpinnerService } from '../../_services/spinner.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { ItemManagementService } from '../../_services/item-management.service';
import { invalid } from '@angular/compiler/src/render3/view/util';

@Component({
  selector: 'app-reply-message',
  templateUrl: './reply-message.component.html',
  styleUrls: ['./reply-message.component.css']
})
export class ReplyMessageComponent implements OnInit {
  popUpForm: FormGroup;

  constructor
    (
      private SpinnerService: SpinnerService,
      private dialogRef: MatDialogRef<ReplyMessageComponent>,
      private formBuilder: FormBuilder,
      private itemManagementService: ItemManagementService,
      private authenticationService: AuthenticationService,
      @Optional() @Inject(MAT_DIALOG_DATA) public data: MessageModel,
      @Inject('APIUrl') private APIUrl: string
    ) {
  }

  ngOnInit() {
    this.popUpForm = this.formBuilder.group({
      body: ['', Validators.required]
    });
  }

  onSubmit(messageModel: MessageModel) {
    var fromBuyer = '0';
    if (this.popUpForm.invalid) {
      return;
    }
    else {
      this.SpinnerService.show();
      if (this.authenticationService.currentUserValue.id == messageModel.buyerId) {
        fromBuyer = '1';
      }
      this.itemManagementService.sendMessage(messageModel.buyerId, fromBuyer, messageModel.itemModel.id, this.popUpForm.controls['body'].value).subscribe(res => {
        if (res != undefined) {
          this.setStatusRead(messageModel);
          alert("Üzenet elküldve.");
          this.dialogRef.close(res);
        } else {
          alert("Ez most nem sikerült.");
        }
        this.SpinnerService.hide();
      });
    }
  }

  setStatusRead(msg) {
    this.SpinnerService.show();
    this.itemManagementService.updateMessageStatus(msg.id, '0').subscribe(res => {
      this.SpinnerService.hide();
    });
  }
}
