import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItemManagementService } from '../../_services/item-management.service';
import { MessageModel } from '../../_models/item-management.model';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { SpinnerService } from '../../_services/spinner.service';
import { ReplyMessageComponent } from '../reply-message/reply-message.component';
import { map, findIndex } from 'rxjs/operators';
import { AuthenticationService } from '../../_services/authentication.service';
import { User } from '../../_models/user'
import { SelectionModel } from '@angular/cdk/collections';
import { ConfirmationDialogComponent } from '../../confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-list-message',
  templateUrl: './list-message.component.html',
  styleUrls: ['./list-message.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ListMessageComponent implements OnInit {
  returnUrl: string;
  msgList: Array<MessageModel> = [] as Array<MessageModel>;
  msgList2: Array<MessageModel> = [] as Array<MessageModel>;
  msgList3: Array<MessageModel> = [] as Array<MessageModel>;

  userList: Array<User> = [] as Array<User>;
  displayedColumns: string[] = ['select', 'username', 'email'];
  dataSource = new MatTableDataSource<User>();
  selection = new SelectionModel<User>(true, []);
  isAdmin = false;
  selectedMessageTab: string = 'Incoming';

  constructor(
    private route: ActivatedRoute,
    private itemManagementService: ItemManagementService,
    private authenticationService: AuthenticationService,
    private SpinnerService: SpinnerService,
    public dialog: MatDialog,
    @Inject('APIUrl') private APIUrl: string
  ) { }

  onSendInMail(body: string) {
    this.itemManagementService.sendInMail(body, this.selection.selected).subscribe();
    alert("Üzenet elküldve.");
  }
  onSendEmail(body: string) {
    this.itemManagementService.sendEmail(body, this.selection.selected).subscribe();
    alert("Üzenet elküldve.");
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ? this.selection.clear() : this.dataSource.data.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: User, index?: number): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${index + 1}`;
  }

  ngOnInit() {
    this.getFreshRecords();

    //Bejövő
    this.itemManagementService.getMessagesbyUser().pipe(map(data => data.filter(fdata =>
      (fdata.isFromBuyer == true && fdata.isDeletedByItemOwner == false && fdata.buyerId != this.authenticationService.currentUserValue.id && fdata.body != "Adásvétel emlékeztető")
      || (fdata.isFromBuyer == false && fdata.isDeletedByBuyer == false && fdata.buyerId == this.authenticationService.currentUserValue.id && fdata.body != "Adásvétel emlékeztető")
    )))
      .subscribe(result => {
        this.msgList = result;
      });

    //Kimenő
    this.itemManagementService.getMessagesbyUser().pipe(map(data => data.filter(fdata =>
      (fdata.isFromBuyer == true && fdata.isDeletedByBuyer == false && fdata.buyerId == this.authenticationService.currentUserValue.id && fdata.body != "Adásvétel emlékeztető")
      || (fdata.isFromBuyer == false && fdata.isDeletedByItemOwner == false && fdata.buyerId != this.authenticationService.currentUserValue.id && fdata.body != "Adásvétel emlékeztető")
    )))
      .subscribe(result => {
        this.msgList2 = result;
      });

    //Adás-vétel
    this.itemManagementService.getMessagesbyUser().pipe(map(data => data.filter(fdata =>
      fdata.body == "Adásvétel emlékeztető")))
      .subscribe(result => {
        this.msgList3 = result;
      });

    this.authenticationService.getAll().subscribe(res => {
      this.dataSource = new MatTableDataSource<User>(res);
    });

    this.authenticationService.getUser().subscribe(res => {
      if (res.username == "BartermesterAdmin" || res.username == "peterb90" || res.username == "peterb1990" || res.username == "BoomBoomBetty" || res.username == "chikani") {
        this.isAdmin = true;
      }
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  getFreshRecords() {
    this.itemManagementService.setMessagesbyUser();
  }

  openDialog(obj) {
    const dialogRef = this.dialog.open(ReplyMessageComponent, {
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.getFreshRecords();
      }
    });
  }

  setStatusNew(msg) {
    this.SpinnerService.show();
    this.itemManagementService.updateMessageStatus(msg.id, '1').subscribe(res => {
      this.SpinnerService.hide();
      this.getFreshRecords();
    });
  }

  setStatusRead(msg) {
    this.SpinnerService.show();
    this.itemManagementService.updateMessageStatus(msg.id, '0').subscribe(res => {
      this.SpinnerService.hide();
      this.getFreshRecords();
    });
  }

  setStatusDeleted(msg) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result == "confirmed") {
        this.SpinnerService.show();

        var fisDeletedByBuyer = '0';
        var fisDeletedByItemOwner = '0';

        if (msg.isDeletedByBuyer == true) {
          fisDeletedByBuyer = '1'
        }

        if (msg.isDeletedByItemOwner == true) {
          fisDeletedByItemOwner = '1'
        }

        //If Item owner then  isDeletedByItemOwner = '1'
        if (msg.buyerId != this.authenticationService.currentUserValue.id) {
          fisDeletedByItemOwner = '1'
        }

        //If buyer then  isDeletedByBuyer = '1'
        if (msg.buyerId == this.authenticationService.currentUserValue.id) {
          fisDeletedByBuyer = '1'
        }

        this.itemManagementService.updateMessageDeletedStatus(msg.id, fisDeletedByBuyer, fisDeletedByItemOwner).subscribe(res => {
          this.SpinnerService.hide();
          this.getFreshRecords();
        });
      }
    });
  }

  messageHeaderClick(type: string) {
    this.selectedMessageTab = type;
  }
}

