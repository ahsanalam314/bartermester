import { Component, Optional, Inject } from '@angular/core';
import { ItemManagementService } from '../../_services/item-management.service';
import { ItemModel, ImageModel } from '../../_models/item-management.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MetaDataService } from '../../_services/meta-data.service';
import { LocationModel, ConditionModel, CategoryModel } from '../../_models/meta-data.model';
import { SpinnerService } from '../../_services/spinner.service';
import { isNullOrUndefined } from 'util';
import { NgxImageCompressService } from 'ngx-image-compress';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})
export class EditItemComponent {
  action: string;
  local_data: any;
  popUpForm: FormGroup;
  locationList: Array<LocationModel> = [] as Array<LocationModel>;
  conditionList: Array<ConditionModel> = [] as Array<ConditionModel>;
  categoryList: Array<CategoryModel> = [] as Array<CategoryModel>;
  imageFiles: Array<ImageModel> = [] as Array<ImageModel>;

  selectedFiles: File[] = [];
  file: any;
  localCompressedURl: any;
  listURL: Array<any> = [] as Array<any>;
  imgResultAfterCompress: string;

  constructor(
    private SpinnerService: SpinnerService,
    private ItemManagementService: ItemManagementService,
    private metaDataService: MetaDataService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditItemComponent>,
    private imageCompress: NgxImageCompressService,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: ItemModel,
    @Inject('APIUrl') private APIUrl: string
  ) {
    this.metaDataService.getAllLocation().subscribe(res => this.locationList = res as []);
    this.metaDataService.getAllCondition().subscribe(res => this.conditionList = res as []);
    this.metaDataService.getAllCategory().subscribe(res => this.categoryList = res as []);

    this.local_data = { ...data };
    this.action = this.local_data.action;
    this.popUpForm = this.formBuilder.group({
      id: [data.id],
      name: [data.name, Validators.required],
      singlePrice: [data.singlePrice, Validators.required],
      unitNumber: [data.unitNumber, Validators.required],
      discount: [data.discount, Validators.required],
      age: [data.age],
      description: [data.description],
      locationModelId: [data.locationModelId, Validators.required],
      conditionModelId: [data.conditionModelId, Validators.required],
      categoryModelId: [data.categoryModelId, Validators.required],
      userName: [data.user.username],
      userId: [data.userId],
      statusModelGuid: [data.statusModelGuid],
      postedAt: [data.postedAt],
      transportModel: [data.transportModel, Validators.required],
    });

    this.getImages();
  }

  onSubmit() {
    if (this.popUpForm.invalid) {
      return;
    }
    this.SpinnerService.show();
    
    this.ItemManagementService.modifyItem(this.popUpForm.value as ItemModel).subscribe(data => {      
      //Upload pictures
      if (this.selectedFiles.length > 0) {
        this.ItemManagementService.createImagebyItem(data.id, this.selectedFiles);
      }
      this.dialogRef.close(data);
      this.SpinnerService.hide();
      //alert("Mentés kész.");
    });
  }

  getImages() {
    this.SpinnerService.show();
    this.ItemManagementService.getImagesbyItem(this.data.id).subscribe(res => {
      if (!isNullOrUndefined(res)) {
        this.imageFiles = res;
      }
      //else {  }
      this.SpinnerService.hide();
    });
  }

  selectFile(event) {
    if (this.selectedFiles.length >= 5 || event.target.files.length >= 6) {
      alert("Maximum 5 képet tölthetsz fel!");
      return;
    }

    if (event.target.files) {
      for (let i = 0; i < event.target.files.length; i++) {
        this.file = event.target.files[i];

        if (this.file.type.match('image.*')) {
          if (this.file.size >= 2 ** 23) {
            alert("Túl nagy kép: " + this.file.name + " " + (this.file.size / (2 ** 20)).toFixed(2) + " MB. Maximális méret: 8.00 MB!");
          } else {

            var reader = new FileReader();
            reader.readAsDataURL(this.file);
            reader.onload = (event: any) => {
              this.compressFile(event.target.result, this.file['name']);
            }

          }
        } else {
          alert('Érvénytelen képformátum!');
        }
      }
    }
  }

  compressFile(image, fileName: string) {
    //console.log(fileName);
    var orientation = -1;

    console.log(this.file.size);
    if (this.file.size <= 500000) {
      this.imgResultAfterCompress = image;
      this.localCompressedURl = image;
      this.listURL.push(this.localCompressedURl);
      // create file from byte
      // call method that creates a blob from dataUri
      const imageBlob = this.dataURItoBlob(this.imgResultAfterCompress.split(',')[1]);
      //imageFile created below is the new compressed file which can be send to API in form data
      const imageFile = new File([imageBlob], fileName, { type: 'image/jpeg' });

      this.selectedFiles.push(imageFile);
      console.log(this.selectedFiles);
    } else {

      this.imageCompress.compressFile(image, orientation, 50, 50).then(
        result => {
          this.imgResultAfterCompress = result;
          this.localCompressedURl = result;
          this.listURL.push(this.localCompressedURl);
          // create file from byte
          // call method that creates a blob from dataUri
          const imageBlob = this.dataURItoBlob(this.imgResultAfterCompress.split(',')[1]);
          //imageFile created below is the new compressed file which can be send to API in form data
          const imageFile = new File([imageBlob], fileName, { type: 'image/jpeg' });

          this.selectedFiles.push(imageFile);
          console.log(this.selectedFiles);
        });
    }
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  clickDelete(index: number) {
    this.listURL.splice(index, 1);
    this.selectedFiles.splice(index, 1);
    console.log(this.selectedFiles);
  }

  clickDeleteImagefromServer(imageId: string) {
    this.SpinnerService.show();
    this.ItemManagementService.deleteImageModel(imageId).subscribe(res => {
      this.getImages();
      this.SpinnerService.hide();
    });
    console.log(imageId);
  }

  displayFn(locationModel: LocationModel): string {
    return locationModel && locationModel.city ? locationModel.city : '';
  }
}
