import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewItemComponent } from './new-item/new-item.component';
import { BackpackRoutingModule } from './backpack-rounting.module';
import { MaterialModule } from '../material.module';
import { ListItemComponent } from './list-item/list-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditItemComponent } from './edit-item/edit-item.component';
import { ListMessageComponent } from './list-message/list-message.component';
import { ReplyMessageComponent } from './reply-message/reply-message.component';
import { CreateDemandComponent } from './demand-backpack/create-demand/create-demand.component';
import { ModifyDemandComponent } from './demand-backpack/modify-demand/modify-demand.component';
import { DemandListComponent } from './demand-backpack/demand-list/demand-list.component';



@NgModule({
  declarations: [NewItemComponent, ListItemComponent, EditItemComponent, ListMessageComponent, ReplyMessageComponent, DemandListComponent, CreateDemandComponent, ModifyDemandComponent],
  entryComponents: [NewItemComponent, EditItemComponent, ReplyMessageComponent, CreateDemandComponent, ModifyDemandComponent],
  imports: [
    CommonModule,
    BackpackRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class BackpackModule { }
