import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItemManagementService } from '../../_services/item-management.service';
import { ItemModel, ImageModel } from '../../_models/item-management.model';
import { MatDialog } from '@angular/material';
import { EditItemComponent } from '../edit-item/edit-item.component';
import { NewItemComponent } from '../new-item/new-item.component';
import { SpinnerService } from '../../_services/spinner.service';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
  returnUrl: string;
  tiles: Array<ItemModel> = [] as Array<ItemModel>;
  imageFiles: Array<ImageModel> = [] as Array<ImageModel>;

  constructor(
    private route: ActivatedRoute,
    private itemManagementService: ItemManagementService,
    private SpinnerService: SpinnerService,
    public dialog: MatDialog,
    @Inject('APIUrl') private APIUrl: string
  ) {
    this.getFreshRecords();

    this.itemManagementService.getBackpackItem().subscribe(
      result => {
        if (result != undefined) {
          this.tiles = result;
        }        
      });

    this.itemManagementService.getImagesbyUser().subscribe(
      result => {
        if (result != undefined) {
          this.imageFiles = result;
        }
      });
  };

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  getFreshRecords() {
    this.itemManagementService.setBackpackItem();    
  }

  openDialog(obj) {
    const dialogRef = this.dialog.open(EditItemComponent, {
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.getFreshRecords();
      }
    });
  }

  onNewRecordClick() {
    const dialogRef = this.dialog.open(NewItemComponent, {})
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  setActive(CurrentItemId: string) {
    this.SpinnerService.show();
    this.itemManagementService.setItemStatus('4E94586C-724E-44DD-A611-671D4DA06ED6', CurrentItemId).subscribe(res => {
      this.SpinnerService.hide();
      this.getFreshRecords();
    });
  }

  setInactive(CurrentItemId: string) {
    this.SpinnerService.show();
    this.itemManagementService.setItemStatus('26952B18-B2B3-4C30-8191-966EB041ACAD', CurrentItemId).subscribe(res => {
      this.SpinnerService.hide();
      this.getFreshRecords();
    });
  }

  clickDelete(CurrentItemId: string) {
    if (confirm("Biztosan törlöd a terméket?")) {
      this.SpinnerService.show();
      this.itemManagementService.deleteItem(CurrentItemId).subscribe(res => {        
        this.getFreshRecords();
        this.SpinnerService.hide();
      });
    }
  }

  setClasses(statusId: number) {
    let myClasses = {
      status_background_draft: statusId === 0,
      status_background_active: statusId === 1,
      status_background_inactive: statusId === 2,
      status_background_sold: statusId === 3,
    };
    return myClasses;
  }

  setClassesBorder(statusId: number) {
    let myClasses = {
      status_border_draft: statusId === 0,
      status_border_active: statusId === 1,
      status_border_inactive: statusId === 2,
      status_border_sold: statusId === 3,
    };
    return myClasses;
  }
}
