import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ItemManagementService } from '../../_services/item-management.service';
import { CreateItemModel } from '../../_models/item-management.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MatDatepicker } from '@angular/material';
import { MetaDataService } from '../../_services/meta-data.service';
import { LocationModel, ConditionModel, CategoryModel } from '../../_models/meta-data.model';
import { User } from '../../_models/user';
import { AuthenticationService } from '../../_services/authentication.service';
import { SpinnerService } from '../../_services/spinner.service';
import { NgxImageCompressService } from 'ngx-image-compress';

@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.css']
})
export class NewItemComponent implements OnInit {
  popUpForm: FormGroup;
  locationList: Array<LocationModel> = [] as Array<LocationModel>;
  conditionList: Array<ConditionModel> = [] as Array<ConditionModel>;
  categoryList: Array<CategoryModel> = [] as Array<CategoryModel>;
  currentUser: User;

  selectedFiles: File[] = [];
  file: any;
  localCompressedURl: any;
  listURL: Array<any> = [] as Array<any>;
  imgResultAfterCompress: string;

  constructor(
    private SpinnerService: SpinnerService,
    private ItemManagementService: ItemManagementService,
    private metaDataService: MetaDataService,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private dialogRef: MatDialogRef<NewItemComponent>,
    private imageCompress: NgxImageCompressService
  ) {
    this.authenticationService.currentUser.subscribe(res => this.currentUser = res);

    this.popUpForm = this.formBuilder.group({
      name: ['', Validators.required],
      singlePrice: [1, Validators.required],
      unitNumber: [1, Validators.required],
      discount: [0, Validators.required],
      age: [''],
      description: [''],
      locationModelId: ['', Validators.required],
      conditionModelId: ['', Validators.required],
      categoryModelId: ['', Validators.required],
      userName: [this.currentUser.username],
      transportModel: ['', Validators.required],
      //postedAt: [Date]
    });
  }

  ngOnInit() {
    this.metaDataService.getAllLocation().subscribe(res => this.locationList = res as []);
    this.metaDataService.getAllCondition().subscribe(res => this.conditionList = res as []);
    this.metaDataService.getAllCategory().subscribe(res => this.categoryList = res as []);
  }

  selectFile(event) {
    if (this.selectedFiles.length >= 5 || event.target.files.length >= 6) {
      alert("Maximum 5 képet tölthetsz fel!");
      return;
    }

    if (event.target.files) {
      for (let i = 0; i < event.target.files.length; i++) {
        this.file = event.target.files[i];

        if (this.file.type.match('image.*')) {
          if (this.file.size >= 2 ** 23) {
            alert("Túl nagy kép: " + this.file.name + " " + (this.file.size / (2 ** 20)).toFixed(2) + " MB. Maximális méret: 8.00 MB!");
          } else {

            var reader = new FileReader();
            reader.readAsDataURL(this.file);
            reader.onload = (event: any) => {
              this.compressFile(event.target.result, this.file['name']);
            }

          }
        } else {
          alert('Érvénytelen képformátum!');
        }
      }
    }
  }

  compressFile(image, fileName: string) {
    //console.log(fileName);
    var orientation = -1;

    console.log(this.file.size);
    if (this.file.size <= 500000) {
      this.imgResultAfterCompress = image;
      this.localCompressedURl = image;
      this.listURL.push(this.localCompressedURl);
      // create file from byte
      // call method that creates a blob from dataUri
      const imageBlob = this.dataURItoBlob(this.imgResultAfterCompress.split(',')[1]);
      //imageFile created below is the new compressed file which can be send to API in form data
      const imageFile = new File([imageBlob], fileName, { type: 'image/jpeg' });

      this.selectedFiles.push(imageFile);
      console.log(this.selectedFiles);
    } else {

      this.imageCompress.compressFile(image, orientation, 50, 50).then(
        result => {
          this.imgResultAfterCompress = result;
          this.localCompressedURl = result;
          this.listURL.push(this.localCompressedURl);
          // create file from byte
          // call method that creates a blob from dataUri
          const imageBlob = this.dataURItoBlob(this.imgResultAfterCompress.split(',')[1]);
          //imageFile created below is the new compressed file which can be send to API in form data
          const imageFile = new File([imageBlob], fileName, { type: 'image/jpeg' });

          this.selectedFiles.push(imageFile);
          console.log(this.selectedFiles);
        });
    }
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  onSubmit() {
    if (this.popUpForm.invalid) {
      return;
    }
    this.SpinnerService.show();
    this.ItemManagementService.createItem(this.popUpForm.value as CreateItemModel).subscribe(data => {
      this.ItemManagementService.recordlistBackpack$.next([data, ...this.ItemManagementService.recordlistBackpack$.value]);
      //Upload pictures
      if (this.selectedFiles.length > 0) {
        this.ItemManagementService.createImagebyItem(data.id, this.selectedFiles);
      }

      this.dialogRef.close(data);
      this.SpinnerService.hide();
    });
  }

  clickDelete(index: number) {
    this.listURL.splice(index,1);
    this.selectedFiles.splice(index,1);
    console.log(this.selectedFiles);
  }

}
