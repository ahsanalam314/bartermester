import { Component, OnInit, Optional, Inject } from '@angular/core';
import { CategoryModel } from '../../../_models/meta-data.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DemandManagementService } from '../../../_services/demand-management.service';
import { Demand } from '../../../_models/demand-management.model';
import { SpinnerService } from '../../../_services/spinner.service';
import { MetaDataService } from '../../../_services/meta-data.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modify-demand',
  templateUrl: './modify-demand.component.html',
  styleUrls: ['./modify-demand.component.css']
})
export class ModifyDemandComponent implements OnInit {
  action: string;
  local_data: any;
  popUpForm: FormGroup;
  categoryList: Array<CategoryModel> = [] as Array<CategoryModel>;
  constructor(
    private spinnerService: SpinnerService,
    private demandManagementService: DemandManagementService,
    private metaDataService: MetaDataService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ModifyDemandComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Demand,
    @Inject('APIUrl') private APIUrl: string
  ) {
    this.local_data = { ...data };
    this.action = this.local_data.action;
    this.popUpForm = this.formBuilder.group({
      id: [data.id],
      name: [data.name, Validators.required],
      categoryModelId: [data.categoryModelId, Validators.required],
      userName: [data.user.username],
      userId: [data.userId],
      statusModelGuid: [data.statusModelGuid],
      postedAt: [data.postedAt],
      description: [data.description]
    });
  }

  ngOnInit() {
    this.metaDataService.getAllCategory().subscribe(res => this.categoryList = res as []);
  }
  onSubmit() {
    if (this.popUpForm.invalid) {
      return;
    }
    this.spinnerService.show();
    this.demandManagementService.setDemand(this.popUpForm.value as Demand).subscribe(data => {
      this.dialogRef.close(data);
      this.spinnerService.hide();
      alert("Mentés kész.");
    });
  }
}
