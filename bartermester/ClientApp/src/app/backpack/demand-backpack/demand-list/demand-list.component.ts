import { Component, OnInit, Inject } from '@angular/core';
import { ModifyDemandComponent } from '../modify-demand/modify-demand.component';
import { CreateDemandComponent } from '../create-demand/create-demand.component';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { SpinnerService } from '../../../_services/spinner.service';
import { Demand } from '../../../_models/demand-management.model';
import { DemandManagementService } from '../../../_services/demand-management.service';

@Component({
  selector: 'app-demand-list',
  templateUrl: './demand-list.component.html',
  styleUrls: ['./demand-list.component.css']
})
export class DemandListComponent implements OnInit {
  returnUrl: string;
  demands: Array<Demand> = [] as Array<Demand>;
  constructor(private route: ActivatedRoute,
    private demandManagementService: DemandManagementService,
    private spinnerService: SpinnerService,
    public dialog: MatDialog,
    @Inject('APIUrl') private APIUrl: string) {
    //this.getFreshRecords();
    this.demandManagementService.getDemandsByUser().subscribe(
      result => {
        if (result != undefined) {
          this.demands = result;
        }
      });
  };

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  getFreshRecords() {
    this.demandManagementService.getDemandsByUser().subscribe(
      result => {
        if (result != undefined) {
          this.demands = result;
        }
      });
  };

  openDialog(obj) {
    const dialogRef = this.dialog.open(ModifyDemandComponent, {
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.getFreshRecords();
      }
    });
  }

  onNewRecordClick() {
    const dialogRef = this.dialog.open(CreateDemandComponent, {})
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  setActive(currentDemandId: string) {
    this.spinnerService.show();
    this.demandManagementService.setDemandStatus(1, currentDemandId).subscribe(res => {
      this.spinnerService.hide();
      this.getFreshRecords();
    });
  }

  setInactive(currentDemandId: string) {
    this.spinnerService.show();
    this.demandManagementService.setDemandStatus(2, currentDemandId).subscribe(res => {
      this.spinnerService.hide();
      this.getFreshRecords();
    });
  }

  clickDelete(currentDemandId: string) {
    if (confirm("Biztosan törlöd a terméket?")) {
      this.spinnerService.show();
      this.demandManagementService.deleteDemand(currentDemandId).subscribe(res => {
        this.getFreshRecords();
        this.spinnerService.hide();
      });
    }
  }

  setClasses(statusId: number) {
    let myClasses = {
      status_background_draft: statusId === 0,
      status_background_active: statusId === 1,
      status_background_inactive: statusId === 2,
      status_background_sold: statusId === 3,
    };
    return myClasses;
  }

  setClassesBorder(statusId: number) {
    let myClasses = {
      status_border_draft: statusId === 0,
      status_border_active: statusId === 1,
      status_border_inactive: statusId === 2,
      status_border_sold: statusId === 3,
    };
    return myClasses;
  }
}
