import { Component, OnInit, Optional, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CategoryModel } from '../../../_models/meta-data.model';
import { User } from '../../../_models/user';
import { SpinnerService } from '../../../_services/spinner.service';
import { DemandManagementService } from '../../../_services/demand-management.service';
import { MetaDataService } from '../../../_services/meta-data.service';
import { AuthenticationService } from '../../../_services/authentication.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DemandDto, Demand } from '../../../_models/demand-management.model';

@Component({
  selector: 'app-create-demand',
  templateUrl: './create-demand.component.html',
  styleUrls: ['./create-demand.component.css']
})
export class CreateDemandComponent implements OnInit {
  popUpForm: FormGroup;
  categoryList: Array<CategoryModel> = [] as Array<CategoryModel>;
  currentUser: User;

  constructor(
    private spinnerService: SpinnerService,
    private demandManagementService: DemandManagementService,
    private metaDataService: MetaDataService,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private dialogRef: MatDialogRef<CreateDemandComponent>
  ) {
    this.authenticationService.currentUser.subscribe(res => this.currentUser = res);

    this.popUpForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      categoryModelId: ['', Validators.required],
      userName: [this.currentUser.username]
    });
  }

  ngOnInit() {
    this.metaDataService.getAllCategory().subscribe(res => this.categoryList = res as []);
  }
  onSubmit() {
    if (this.popUpForm.invalid) {
      return;
    }
    this.spinnerService.show();
    this.demandManagementService.setNewDemand(this.popUpForm.value as DemandDto).subscribe(data => {
      this.demandManagementService.recordlistOwnedDemands$.next([data, ...this.demandManagementService.recordlistOwnedDemands$.value]);
      this.dialogRef.close(data);
      this.spinnerService.hide();
    });
  }
}
