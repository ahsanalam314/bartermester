import { NgModule } from '@angular/core';
import { NewItemComponent } from './new-item/new-item.component';
import { Routes, RouterModule } from '@angular/router';
import { ListItemComponent } from './list-item/list-item.component';
import { EditItemComponent } from './edit-item/edit-item.component';
import { ListMessageComponent } from './list-message/list-message.component';
import { ReplyMessageComponent } from './reply-message/reply-message.component';
import { ModifyDemandComponent } from './demand-backpack/modify-demand/modify-demand.component';
import { CreateDemandComponent } from './demand-backpack/create-demand/create-demand.component';
import { DemandListComponent } from './demand-backpack/demand-list/demand-list.component';

const routes: Routes = [
  { path: 'NewItem', component: NewItemComponent },
  { path: 'EditItem', component: EditItemComponent },
  { path: 'ListItem', component: ListItemComponent },
  { path: 'ListMessage', component: ListMessageComponent },
  { path: 'ReplyMessage', component: ReplyMessageComponent },
  { path: 'DemandList', component: DemandListComponent },
  { path: 'ModifyDemand', component: ModifyDemandComponent },
  { path: 'CreateDemand', component: CreateDemandComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackpackRoutingModule { }

