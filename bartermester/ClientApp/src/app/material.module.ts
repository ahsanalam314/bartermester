import { NgModule, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCarouselModule } from '@ngmodule/material-carousel';
import { ImageViewerModule } from '@hallysonh/ngx-imageviewer';
import { NgxGalleryModule } from 'ngx-gallery';

import {
  MatButtonModule,
  MatMenuModule,
  MatIconModule,
  MatCardModule,
  MatSidenavModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatTooltipModule,
  MatToolbarModule,
  MatStepperModule,
  MatDialogModule,
  MatTableModule,
  MatSnackBarModule,
  MatSelectModule,
  MatPaginatorModule,
  MatTreeModule,
  MatTabsModule,
  MatSortModule,
  MatDatepickerModule, MatNativeDateModule,
  MatIconRegistry,
  MatProgressBarModule,
  MatGridListModule,
  MatDividerModule,
  MatSlideToggleModule,
  MAT_DATE_LOCALE, MAT_DATE_FORMATS, DateAdapter, MatAutocompleteModule
} from '@angular/material';
import {
  MatMomentDateModule,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MAT_MOMENT_DATE_FORMATS
} from "@angular/material-moment-adapter";
import 'moment/locale/hu';
import { DomSanitizer } from '@angular/platform-browser';

@NgModule({
  declarations: [],
  imports: [
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatCardModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatToolbarModule,
    MatStepperModule,
    MatDialogModule,
    MatTableModule,
    MatSnackBarModule,
    MatSelectModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule, MatMomentDateModule,
    MatGridListModule,
    MatAutocompleteModule,
    MatDividerModule,
    MatSlideToggleModule,
    CommonModule,
    MatCarouselModule,
    ImageViewerModule,
    NgxGalleryModule
  ],
  providers: [
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { strict: true } },
    { provide: MAT_DATE_LOCALE, useValue: 'hu' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
  exports: [
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatCardModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatInputModule,
    MatTooltipModule,
    MatToolbarModule,
    MatStepperModule,
    MatDialogModule,
    MatTableModule,
    MatSnackBarModule,
    MatSelectModule,
    MatPaginatorModule,
    MatTreeModule,
    MatTabsModule,
    MatSortModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatGridListModule,
    MatProgressBarModule,
    MatAutocompleteModule,
    MatDividerModule,
    MatSlideToggleModule,
    MatCarouselModule,
    ImageViewerModule,
    NgxGalleryModule
  ],
})
export class MaterialModule {
  constructor(
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    @Inject('APIUrl') private APIUrl: string)
  {
    iconRegistry.addSvgIcon('home_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/1_home.svg`));
    iconRegistry.addSvgIcon('backpack_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/2_backpack.svg`));
    iconRegistry.addSvgIcon('invite_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/3_meghivo_best.svg`));
    iconRegistry.addSvgIcon('bm_logo', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/4_bmlogo_best_withoutM.svg`));
    iconRegistry.addSvgIcon('message_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/5_envelope.svg`));
    iconRegistry.addSvgIcon('balance_pic', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/balance.svg`));
    iconRegistry.addSvgIcon('balance_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/egyenleged.svg`));
    iconRegistry.addSvgIcon('refresh_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/refresh.svg`));
    iconRegistry.addSvgIcon('wanted_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/wanted.svg`));

    iconRegistry.addSvgIcon('1_home_black_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/1_home_black_icon.svg`));
    iconRegistry.addSvgIcon('1_home_magenta_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/1_home_magenta_icon.svg`));
    iconRegistry.addSvgIcon('2_backpack_black_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/2_backpack_black_icon.svg`));
    iconRegistry.addSvgIcon('2_backpack_magenta_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/2_backpack_magenta_icon.svg`));
    iconRegistry.addSvgIcon('3_purchased_black_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/3_purchased_black_icon.svg`));
    iconRegistry.addSvgIcon('3_purchased_magenta_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/3_purchased_magenta_icon.svg`));
    iconRegistry.addSvgIcon('5_invitation_black_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/5_invitation_black_icon.svg`));
    iconRegistry.addSvgIcon('5_invitation_magenta_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/5_invitation_magenta_icon.svg`));
    iconRegistry.addSvgIcon('6_nightswitch_black_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/6_nightswitch_black_icon.svg`));
    iconRegistry.addSvgIcon('6_nightswitch_magenta_icon', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/6_nightswitch_magenta_icon.svg`));
    iconRegistry.addSvgIcon('7_bmlogo_nightswitch', sanitizer.bypassSecurityTrustResourceUrl(`${this.APIUrl}/assets/7_bmlogo_nightswitch.svg`));
  }
}
