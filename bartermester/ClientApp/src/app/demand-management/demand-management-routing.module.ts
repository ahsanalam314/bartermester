import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemandMarketplaceComponent } from './demand-marketplace/demand-marketplace.component';
import { ViewDemandComponent } from './view-demand/view-demand.component';
import { OfferListComponent } from './offer-list/offer-list.component';
import { CreateOfferComponent } from './create-offer/create-offer.component';

const routes: Routes = [
  { path: 'DemandMarketplace', component: DemandMarketplaceComponent },
  { path: 'ViewDemand', component: ViewDemandComponent },
  { path: 'OfferList', component: OfferListComponent },
  { path: 'CreateOffer', component: CreateOfferComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemandManagementRoutingModule { }
