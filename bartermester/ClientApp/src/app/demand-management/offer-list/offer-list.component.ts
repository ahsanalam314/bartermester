import {
  Component, OnInit, Inject, Optional, Injectable } from '@angular/core';
import { Offer, Demand, OfferImage } from '../../_models/demand-management.model';
import { User } from '../../_models/user';
import { MatTableDataSource, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { DemandManagementService } from '../../_services/demand-management.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { SpinnerService } from '../../_services/spinner.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { async } from '@angular/core/testing';
@Component({
  selector: 'app-offer-list',
  templateUrl: './offer-list.component.html',
  styleUrls: ['./offer-list.component.css']
})
@Injectable({
  providedIn: 'root'
})
export class OfferListComponent implements OnInit {
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  galleryImages2: NgxGalleryImage[];
  returnUrl: string;
  offerList: Array<Offer> = [] as Array<Offer>;
  userList: Array<User> = [] as Array<User>;
  imageList: Array<OfferImage> = [] as Array<OfferImage>;
  isAdmin = false;
  havePicture = false;
  isAccepted = false;
  isRejected = false;
  constructor(
    private route: ActivatedRoute,
    private demandManagementService: DemandManagementService,
    private authenticationService: AuthenticationService,
    private spinnerService: SpinnerService,
    public dialog: MatDialog,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Demand,
    @Inject('APIUrl') private APIUrl: string
  ) {
    this.demandManagementService.getImagesByDemand(this.data.id).subscribe(result => {
      this.imageList = result;
      this.galleryImages = this.imageList.map((image) => {
        return {
          small: image.imagePath,
          medium: image.imagePath,
          big: image.imagePath,
          description: image.offerId,
        };
      });
    });
  }

  ngOnInit() {
    this.demandManagementService.getOffersByDemand(this.data.id).subscribe(result => {
      this.offerList = result;
    });
    this.galleryOptions = [
      {
        width: '100%',
        height: '40vh',
        thumbnailsColumns: 3,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      //max-width 800
      {
        breakpoint: 600,
        width: '100%',
        height: '350px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 300,
        preview: false
      }
    ];
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  getGalleryImages(offerId: string) {
    console.log(this.galleryImages);
    this.galleryImages2 = this.galleryImages.filter(e => e.description == offerId);
    console.log(this.galleryImages2);
    return this.galleryImages2;
  }
  getFreshRecords() {
    //this.demandManagementService.setMessagesbyUser();
  }

  //openDialog(obj) {
  //  const dialogRef = this.dialog.open(ReplyMessageComponent, {
  //    data: obj
  //  });
  //  dialogRef.afterClosed().subscribe(result => {
  //    if (result != undefined) {
  //      this.getFreshRecords();
  //    }
  //  });
  //}

  setOfferAccepted(offerId: string) {
    this.spinnerService.show();
    this.demandManagementService.setOfferStatus(1, offerId).subscribe(res => {
      this.spinnerService.hide();
      this.getFreshRecords();
    });
  }

  setOfferRejected(offerId: string) {
    this.spinnerService.show();
    this.demandManagementService.setOfferStatus(2, offerId).subscribe(res => {
      this.spinnerService.hide();
      this.getFreshRecords();
    });
  }

}
