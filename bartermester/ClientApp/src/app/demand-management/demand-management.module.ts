import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemandManagementRoutingModule } from './demand-management-routing.module';
import { DemandMarketplaceComponent } from './demand-marketplace/demand-marketplace.component';
import { ViewDemandComponent } from './view-demand/view-demand.component';
import { OfferListComponent } from './offer-list/offer-list.component';
import { CreateOfferComponent } from './create-offer/create-offer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { NgcCookieConsentModule } from 'ngx-cookieconsent';


@NgModule({
  declarations: [DemandMarketplaceComponent, ViewDemandComponent, OfferListComponent, CreateOfferComponent],
  entryComponents: [ViewDemandComponent, CreateOfferComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    DemandManagementRoutingModule,
    MaterialModule,
    NgcCookieConsentModule
  ]
})
export class DemandManagementModule { }
