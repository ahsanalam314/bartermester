import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { SpinnerService } from '../../_services/spinner.service';
import { Demand } from '../../_models/demand-management.model';
import { DemandManagementService } from '../../_services/demand-management.service';
import { ViewDemandComponent } from '../view-demand/view-demand.component';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-demand-marketplace',
  templateUrl: './demand-marketplace.component.html',
  styleUrls: ['./demand-marketplace.component.css']
})
export class DemandMarketplaceComponent implements OnInit {
  returnUrl: string;
  demands: Array<Demand> = [];
  categoryList: Array<Demand> = [];
  categoryModelId = '';
  categoryReset = '';
  randomNum = 0;
  numofdemands: any;
  numofpseudo: any;
  x = 6;
  y = 0;
  constructor(
    private route: ActivatedRoute,
    private demandManagementService: DemandManagementService,
    private spinnerService: SpinnerService,
    public dialog: MatDialog,
    @Inject('APIUrl') private APIUrl: string
  ) {
    this.demandManagementService.getActiveDemands().subscribe(
      result => {
        if (result != undefined) {
          this.demands = result;
        }
      });
  };

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.getFreshRecords();
  }

  getFreshRecords() {
    this.demandManagementService.getActiveDemands().subscribe(res => {
      if (!isNullOrUndefined(res)) {
        this.demands = res;
        this.numofdemands = this.demands.length;
        this.numofpseudo = Math.round(this.numofdemands / 4);
        this.x = 6;
        this.y = 0;
        if (this.numofdemands == 0) { }
        else {
          for (var i = 0; i < this.numofpseudo; i++) {
            this.demands.splice(Math.floor(Math.random() * (this.x - this.y) + this.y), 0,
              {
                id: 'pseudo', name: "pseudo", description: "", statusModel: this.demands[0].statusModel,
                statusModelGuid: "", postedAt: new Date(), updatedAt: new Date(), categoryModel: this.demands[0].categoryModel,
                categoryModelId: "", userId: "", user: this.demands[0].user
              });
            this.x = this.x + 5;
            this.y = this.y + 5;
          }
          this.categoryList = this.demands.filter((thing, index, self) =>
            self.findIndex(t => t.categoryModelId === thing.categoryModel.id) === index)
            .sort((a, b) => a.categoryModel.name.localeCompare(b.categoryModel.name));
        }
      }
    }); 
  };

  openDialog(obj) {
    const dialogRef = this.dialog.open(ViewDemandComponent, {
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      //if (result != undefined) {
      //  this.getFreshRecords();
      //}
    });
  }
  setClasses(myCategory: string) {
    let myClasses = {
      hex_red: myCategory === 'Divat, ruházat',
      hex_orange: myCategory === 'Otthon, háztartás',
      hex_green: myCategory === 'Szabadidő, sport',
      hex_blue: myCategory === 'Műszaki cikkek, elektronika',
      hex_purple: myCategory === 'Kultúra',
      hex_lightblue: myCategory === 'Baba-mama'
    };
    return myClasses;
  }
  onCategorySelected(event) {
    this.categoryModelId = event.value;
  }
  runFilter() {
    //this.demands = this.myResult;

    if (this.categoryModelId != '') {
      this.demands = this.demands.filter(x => x.categoryModelId == this.categoryModelId);
    } else {
      this.getFreshRecords();
    }
  }
}
