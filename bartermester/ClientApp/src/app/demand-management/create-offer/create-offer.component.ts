import { Component, OnInit, Optional, Inject } from '@angular/core';
import { CategoryModel, LocationModel } from '../../_models/meta-data.model';
import { User } from '../../_models/user';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MetaDataService } from '../../_services/meta-data.service';
import { DemandManagementService } from '../../_services/demand-management.service';
import { SpinnerService } from '../../_services/spinner.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgxImageCompressService } from 'ngx-image-compress';
import { OfferDto, Demand } from '../../_models/demand-management.model';

@Component({
  selector: 'app-create-offer',
  templateUrl: './create-offer.component.html',
  styleUrls: ['./create-offer.component.css']
})
export class CreateOfferComponent implements OnInit {
  popUpForm: FormGroup;
  locationList: Array<LocationModel> = [] as Array<LocationModel>;
  categoryList: Array<CategoryModel> = [] as Array<CategoryModel>;
  currentUser: User;

  selectedFiles: File[] = [];
  file: any;
  localCompressedURl: any;
  listURL: Array<any> = [] as Array<any>;
  imgResultAfterCompress: string;
  constructor(
    private spinnerService: SpinnerService,
    private demandManagementService: DemandManagementService,
    private metaDataService: MetaDataService,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private dialogRef: MatDialogRef<CreateOfferComponent>,
    private imageCompress: NgxImageCompressService,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Demand
  ) {
      this.authenticationService.currentUser.subscribe(res => this.currentUser = res);

    this.popUpForm = this.formBuilder.group({
      name: ['', Validators.required],
      amount: [0, Validators.required],
      transportModel: ['', Validators.required],
      description: [''],
      locationModelId: ['', Validators.required],
      userName: [this.currentUser.username],
      demandId: [data.id]
    }); }

  ngOnInit() {
    this.metaDataService.getAllLocation().subscribe(res => this.locationList = res as []);
  }

  selectFile(event) {
    if (this.selectedFiles.length >= 5 || event.target.files.length >= 6) {
      alert("Maximum 5 képet tölthetsz fel!");
      return;
    }

    if (event.target.files) {
      for (let i = 0; i < event.target.files.length; i++) {
        this.file = event.target.files[i];

        if (this.file.type.match('image.*')) {
          if (this.file.size >= 2 ** 23) {
            alert("Túl nagy kép: " + this.file.name + " " + (this.file.size / (2 ** 20)).toFixed(2) + " MB. Maximális méret: 8.00 MB!");
          } else {

            var reader = new FileReader();
            reader.readAsDataURL(this.file);
            reader.onload = (event: any) => {
              this.compressFile(event.target.result, this.file['name']);
            }

          }
        } else {
          alert('Érvénytelen képformátum!');
        }
      }
    }
  }

  compressFile(image, fileName: string) {
    //console.log(fileName);
    var orientation = -1;

    console.log(this.file.size);
    if (this.file.size <= 500000) {
      this.imgResultAfterCompress = image;
      this.localCompressedURl = image;
      this.listURL.push(this.localCompressedURl);
      // create file from byte
      // call method that creates a blob from dataUri
      const imageBlob = this.dataURItoBlob(this.imgResultAfterCompress.split(',')[1]);
      //imageFile created below is the new compressed file which can be send to API in form data
      const imageFile = new File([imageBlob], fileName, { type: 'image/jpeg' });

      this.selectedFiles.push(imageFile);
      console.log(this.selectedFiles);
    } else {

      this.imageCompress.compressFile(image, orientation, 50, 50).then(
        result => {
          this.imgResultAfterCompress = result;
          this.localCompressedURl = result;
          this.listURL.push(this.localCompressedURl);
          // create file from byte
          // call method that creates a blob from dataUri
          const imageBlob = this.dataURItoBlob(this.imgResultAfterCompress.split(',')[1]);
          //imageFile created below is the new compressed file which can be send to API in form data
          const imageFile = new File([imageBlob], fileName, { type: 'image/jpeg' });

          this.selectedFiles.push(imageFile);
          console.log(this.selectedFiles);
        });
    }
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  onSubmit() {
    if (this.popUpForm.invalid) {
      return;
    }
    this.spinnerService.show();
    this.demandManagementService.setNewOffer(this.popUpForm.value as OfferDto).subscribe(data => {
      //Upload pictures
      if (this.selectedFiles.length > 0) {
        this.demandManagementService.setImagesbyOffer(data.id, this.selectedFiles);
      }
      this.dialogRef.close(data);
      this.spinnerService.hide();
    });
  }

  clickDelete(index: number) {
    this.listURL.splice(index, 1);
    this.selectedFiles.splice(index, 1);
    console.log(this.selectedFiles);
  }

}
