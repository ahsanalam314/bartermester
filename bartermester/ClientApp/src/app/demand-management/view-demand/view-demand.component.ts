import { Component, OnInit, Inject, Optional } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Demand } from '../../_models/demand-management.model';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { AuthenticationService } from '../../_services/authentication.service';
import { DemandManagementService } from '../../_services/demand-management.service';
import { SpinnerService } from '../../_services/spinner.service';
import { ConfirmationDialogComponent } from '../../confirmation-dialog/confirmation-dialog.component';
import { CreateOfferComponent } from '../create-offer/create-offer.component';
import { OfferListComponent } from '../offer-list/offer-list.component';

@Component({
  selector: 'app-view-demand',
  templateUrl: './view-demand.component.html',
  styleUrls: ['./view-demand.component.css']
})
export class ViewDemandComponent implements OnInit {
  action: string;
  local_data: any;
  popUpForm: FormGroup;
  isOwner = false;
  constructor(
    private spinnerService: SpinnerService,
    private dialogRef: MatDialogRef<ViewDemandComponent>,
    private demandManagementService: DemandManagementService,
    private authenticationService: AuthenticationService,
    public dialog: MatDialog,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Demand,
    @Inject('APIUrl') private APIUrl: string
  ) { }

  ngOnInit() {
    this.local_data = { ...this.data };
    this.authenticationService.getUser().subscribe(res => {
      if (res.id == this.data.userId) {
        this.isOwner = true;
      }
    })
  }
  onSubmit(demand: Demand) {
    const dialogRef = this.dialog.open(CreateOfferComponent, {
      data: demand
    });
    dialogRef.afterClosed().subscribe(result => {
      //if (result != undefined) {
      //  this.getFreshRecords();
      //}
    });
  }
  onOfferListClick(demand: Demand) {
    const dialogRef = this.dialog.open(OfferListComponent, {
      data: demand
    });
    dialogRef.afterClosed().subscribe(result => {
      //if (result != undefined) {
      //  this.getFreshRecords();
      //}
    });
  }
}
