export interface User {
  id: string,
  firstName: string,
  lastName: string,
  email: string,
  username: string,
  registeredAt: Date,
  role: string,
  invitorId: string,
  token?: string,
  hasAcceptedGDPR: boolean,
  hasAcceptedTermsAndConditions: boolean
}

export interface UserRegistrationModel {
  username: string,
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  invitorId?: string,
  hasAcceptedGDPR: boolean,
  hasAcceptedTermsAndConditions: boolean,
}

export interface ResetPasswordEmailRequest {
  email: string,
}

export interface PasswordChangeRequest {
  newPassword: string,
}

export interface Token {
  id: string,
  tokenDetail: string,
  invitorId: string,
  isUsed: boolean,
  email: string,
}
