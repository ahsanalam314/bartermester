export interface CategoryModel {
  id: string,
  name: string,
  description: string,
  isActive: boolean
}

export interface ConditionModel {
  id: string,
  name: string,
  isActive: boolean
}

export interface LocationModel {
  id: string,
  city: string 
}

export interface StatusModel {
  guid: string,
  id: number,
  name: string
}
