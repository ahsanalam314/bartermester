import { User } from "./user";
import { ItemModel } from "./item-management.model";

export interface Barter {
  id: string,
  userId: string,
  user: User,
  itemModelId: string,
  itemModel: ItemModel,
  buyingPrice: number,
  startedAt: Date,
  closedAt: Date,
  status: string,
  comment: string,
  location: string
}

export interface CreateBarter {
  itemModelId: string,
  comment: string,
  location: string
}

export interface TransactionFlow {
  id: string,
  amount: number,
  transactionType: number,
  userId: string,
  user: User,
  transactionStatus: number,
  barterId: string,
  barter: Barter,
  transactionAt: Date
}
