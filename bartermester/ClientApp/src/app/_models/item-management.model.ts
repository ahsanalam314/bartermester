import { LocationModel, ConditionModel, CategoryModel, StatusModel } from "./meta-data.model";
import { User } from "./user";

export interface ItemModel {
  id: string,
  name: string,
  singlePrice: number,
  unitNumber: number,
  discount: number,
  statusModel: StatusModel,
  statusModelGuid: string,
  age: string,
  description: string,
  postedAt: Date,
  updatedAt: Date,
  locationModelId: string,
  locationModel: LocationModel,
  conditionModelId: string,
  conditionModel: ConditionModel,
  categoryModelId: string,
  categoryModel: CategoryModel,
  userId: string,
  user: User,
  transportModel: string,
  imageUrls: string
}

export interface CreateItemModel {
  name: string,
  singlePrice: number,
  unitNumber: number,
  discount: number,
  age: string,
  description: string,
  locationModelId: string,
  conditionModelId: string,
  categoryModelId: string,
  transportModel: string,
  imageUrls: string
}

export interface ImageModel {
  id: string,
  name: string,
  sequenceId: number,  
  postedAt: Date,  
  itemModelId: string,
  itemModel: ItemModel,
  imagePath: string
}

export interface MessageModel {
  id: string,
  buyerId: string,
  user: User,
  isFromBuyer: boolean,
  itemModelId: string,
  itemModel: ItemModel,
  body: string,
  postedAt: Date,
  isNew: boolean,
  isDeletedByBuyer: boolean,
  isDeletedByItemOwner: boolean
}
