import { CategoryModel, StatusModel, LocationModel } from "./meta-data.model";
import { User } from "./user";

export interface Demand {
  id: string,
  name: string,
  description: string,
  statusModel: StatusModel,
  statusModelGuid: string,
  postedAt: Date,
  updatedAt: Date,
  categoryModelId: string,
  categoryModel: CategoryModel,
  userId: string,
  user: User
}

export interface DemandDto {
  name: string,
  description: string,
  statusModelGuid: string,
  categoryModelId: string,
  userId: string
}
export interface Offer {
  id: string,
  name: string,
  transportModel: string,
  offerStatus: OfferStatus,
  offerStatusGuid: string,
  locationModel: LocationModel,
  locationModelId: string,
  postedAt: Date,
  updatedAt: Date,
  userId: string,
  user: User,
  offerImages: [OfferImage]
}

export interface OfferDto {
  name: string,
  transportModel: string,
  locationModelId: string,
  demandId: string,
  amouint: number
}
export interface OfferImage {
  id: string,
  name: string,
  postedAt: Date,
  offerId: string,
  offer: Offer,
  imagePath: string
}
export interface OfferImageDto {
  name: string
}
export interface OfferStatus {
  guid: string,
  id: number,
  name: string
}
export interface GaleryModel {
  small: string,
  medium: string,
  big: string
}
