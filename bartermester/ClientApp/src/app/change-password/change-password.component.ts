import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../_services/authentication.service';
import { SpinnerService } from '../_services/spinner.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  hide = true;
  hide2 = true;
  changePasswordForm: FormGroup;
  loading = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private SpinnerService: SpinnerService,
  ) {
  }

  ngOnInit() {
    this.changePasswordForm = this.formBuilder.group({
      password: ['', Validators.required],
      passwordconf: ['', Validators.required],
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.changePasswordForm.controls; }

  onSubmit() {
    // stop here if form is invalid
    if (this.changePasswordForm.invalid) {
      alert("A mezők kitöltése kötelező.")
      return;
    }

    //Password confirmation
    if (this.changePasswordForm.controls["password"].value != this.changePasswordForm.controls["passwordconf"].value) {
      alert("Jelszó nem egyezik!");
      return;
    }

    this.SpinnerService.show();
    this.authenticationService.changePassword({ newPassword: this.f.password.value })
      .pipe(first())
      .subscribe(
        data => {
          alert("Az új jelszavad elmentettük.")
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.error = error;
          alert("Ez most nem sikerült.")
          this.SpinnerService.hide();
        });
  }

  onCancel() {
    this.router.navigate([this.returnUrl]);
  }
}
