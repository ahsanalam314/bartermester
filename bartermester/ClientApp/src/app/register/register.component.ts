import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../_services/authentication.service';
import { SpinnerService } from '../_services/spinner.service';
import { first } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { PopupReaderComponent } from '../popup-reader/popup-reader.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  hide = true;
  hide2 = true;
  registerForm: FormGroup;
  loading = false;
  error: any;
  tokenId: string;
  email: string;

  constructor(
    private formBuilder: FormBuilder,
    private SpinnerService: SpinnerService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    public dialog: MatDialog,
  ) {
    this.tokenId = window.location.href.split('tokenId=')[1].toString();

    this.registerForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: [this.email],
      username: ['', Validators.required],
      password: ['', Validators.required],
      passwordconf: [''],
      hasAcceptedGDPR: ['', Validators.requiredTrue],
      hasAcceptedTermsAndConditions: ['', Validators.requiredTrue]
    });
  }

  ngOnInit() {
    this.loading = true;
    this.authenticationService.getTokenDetails(this.tokenId).subscribe(res => {
      this.email = res.email;
      if (res.isUsed) {
        alert("Használt token!");
        this.router.navigate(['/login']);
        this.loading = false;
      }
      this.registerForm.controls['email'].setValue(this.email);
      this.loading = false;
    });
  }

  onSubmit() {
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      alert("Minden mező kitöltése kötelező!");
      return;
    }

    //Password confirmation
    if (this.registerForm.controls["password"].value != this.registerForm.controls["passwordconf"].value) {
      alert("Jelszó nem egyezik!");
      return;
    }

    this.SpinnerService.show();
    //console.log(this.tokenId)
    this.authenticationService.register(this.registerForm.value, this.tokenId)
      .pipe(first())
      .subscribe(res => {
          this.router.navigate(['/']);
        },
        error => {
          this.SpinnerService.hide();
        });
  }

  onGdprClick() {
    const dialogRef = this.dialog.open(PopupReaderComponent, {
      data: 'gdpr'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {

      }
    });
  }

  onTermsOfUseClick() {
    const dialogRef = this.dialog.open(PopupReaderComponent, {
      data: 'termsofuse'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {

      }
    });
  }

  onCancel() {
    this.router.navigate(['/login']);
  }

}
