import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ContactUsComponent } from '../contact-us/contact-us.component';
import { PopupReaderComponent } from '../popup-reader/popup-reader.component';
import { AuthenticationService } from '../_services/authentication.service';
import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  isCurrentUser: boolean;
  userName: Observable<string>;

  constructor(
    public dialog: MatDialog,
    private authenticationService: AuthenticationService,
    @Inject('APIUrl') private APIUrl: string
  ) {
  }

  ngOnInit() {
    this.authenticationService.getUser().subscribe(res => {
      if (isNullOrUndefined(res)) {
        this.isCurrentUser = false;
      } else {
        this.isCurrentUser = true;
      }
    });
  }

  onImpresumClick() {
    const dialogRef = this.dialog.open(PopupReaderComponent, {
      data: 'impressum'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {

      }
    });
  }

  onCopyrightsClick() {
    const dialogRef = this.dialog.open(PopupReaderComponent, {
      data: 'copyright'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {

      }
    });
  }

  onTermsofUsageClick() {
    const dialogRef = this.dialog.open(PopupReaderComponent, {
      data: 'termsofuse'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {

      }
    });
  }

  onGdprClick() {
    const dialogRef = this.dialog.open(PopupReaderComponent, {
      data: 'gdpr'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {

      }
    });
  }

  onContactUsClick() { //  openDialog()
      const dialogRef = this.dialog.open(ContactUsComponent, {      
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result != undefined) {
        
        }
      });
  }

  onFaqClick() {
    const dialogRef = this.dialog.open(PopupReaderComponent, {
      data: 'faq'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {

      }
    });
  }
  
}
