import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-popup-reader',
  templateUrl: './popup-reader.component.html',
  styleUrls: ['./popup-reader.component.css']
})
export class PopupReaderComponent implements OnInit {
  popUpForm: FormGroup;

  constructor
    (
      private dialogRef: MatDialogRef<PopupReaderComponent>,
      private formBuilder: FormBuilder,
      @Optional() @Inject(MAT_DIALOG_DATA) public data: string,
      @Inject('APIUrl') private APIUrl: string
    ) {
  }

  ngOnInit() {
    this.popUpForm = this.formBuilder.group({      
    });
  }
}
