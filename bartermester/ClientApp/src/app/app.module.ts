
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { MaterialModule } from './material.module';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_helpers/auth.guard';
import { RegisterComponent } from './register/register.component';
import { environment } from '../environments/environment';
import { JwtInterceptor } from './_interceptors/jwt.interceptor';
import { ErrorInterceptor } from './_interceptors/error.interceptor';
import { AuthenticationService } from './_services/authentication.service';
import { SpinnerComponent } from './spinner/spinner.component';
import { HomeComponent } from './home/home.component';
import { InviteComponent } from './invite/invite.component';
import { ViewBalanceComponent } from './view-balance/view-balance.component';
import { NgxImageCompressService } from 'ngx-image-compress';
import { FooterComponent } from './footer/footer.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PopupReaderComponent } from './popup-reader/popup-reader.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { NgcCookieConsentModule, NgcCookieConsentConfig } from 'ngx-cookieconsent';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterManagerComponent } from './nav-menu/footer-manager/footer-manager.component';

const cookieConfig: NgcCookieConsentConfig = {
  cookie: {
    domain: 'bartermester.com'
  },
  palette: {
    popup: {
      background: '#000'
    },
    button: {
      background: '#f1d600'
    }
  },
  theme: 'edgeless',
  type: 'opt-out',

  content: {
    message: 'Tájékoztatjuk, hogy a bartermester.com weboldal sütiket használ a weboldal működése, használatának megkönnyítése, a weboldalon végzett tevékenység nyomonkövetése céljából.',
    dismiss: "Értem",
    allow: "Sütik engedélyezése",
    deny: "Sütik elutasítása",
    link: "Részletek",
    href: "https://cookiesandyou.com",
    policy: "Sütik kezelése"
  }
};

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    InviteComponent,
    SpinnerComponent,
    ViewBalanceComponent,
    FooterComponent,
    ContactUsComponent,
    PopupReaderComponent,
    ChangePasswordComponent,
    ForgotPasswordComponent,
    ConfirmationDialogComponent,
    FooterManagerComponent,
  ],
  imports: [
    NgcCookieConsentModule.forRoot(cookieConfig),
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent, /*pathMatch: 'full',*/ canActivate: [AuthGuard] },
      { path: 'register', component: RegisterComponent },
      { path: 'invite', component: InviteComponent, canActivate: [AuthGuard] },
      { path: 'changepassword', component: ChangePasswordComponent, canActivate: [AuthGuard] },
      { path: 'login', component: LoginComponent },
      { path: 'forgotpassword', component: ForgotPasswordComponent },
      { path: 'MarketPlace', loadChildren: () => import('./market-place/market-place.module').then(m => m.MarketPlaceModule), canActivate: [AuthGuard] },
      { path: 'AccountManagement', loadChildren: () => import('./account-management/account-management.module').then(m => m.AccountManagementModule), canActivate: [AuthGuard] },
      { path: 'BarterManagement', loadChildren: () => import('./barter-management/barter-management.module').then(m => m.BarterManagementModule), canActivate: [AuthGuard] },
      { path: 'DemandManagement', loadChildren: () => import('./demand-management/demand-management.module').then(m => m.DemandManagementModule), canActivate: [AuthGuard] },
      { path: 'Backpack', loadChildren: () => import('./backpack/backpack.module').then(m => m.BackpackModule), canActivate: [AuthGuard] },
      { path: 'ViewBalance', component: ViewBalanceComponent, canActivate: [AuthGuard] },
      { path: '**', component: HomeComponent, canActivate: [AuthGuard], redirectTo: '' },
      { path: 'contactUs', component: ContactUsComponent, canActivate: [AuthGuard] },
      { path: 'popupReader', component: PopupReaderComponent, canActivate: [AuthGuard] },

    ]),
    MaterialModule
  ],
  entryComponents: [SpinnerComponent, ConfirmationDialogComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true, deps: [AuthenticationService] },
    { provide: 'APIUrl', useValue: environment.apiUrl },
    NgxImageCompressService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
