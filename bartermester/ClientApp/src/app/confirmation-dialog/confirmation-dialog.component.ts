import { Component, Input, OnInit, Inject } from '@angular/core';
import { PopupReaderComponent } from '../popup-reader/popup-reader.component';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
})
export class ConfirmationDialogComponent implements OnInit {
  @Input() title: string;
  @Input() message: string;

  constructor(
    public dialogRef: MatDialogRef<ConfirmationDialogComponent>
  ) { }

  ngOnInit() { }

  onConfirmClick() {
    this.dialogRef.close("confirmed");
  }

  onCancelClick() {
    this.dialogRef.close();
  }

}
