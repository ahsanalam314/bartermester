import { Component } from '@angular/core';
import { BarterManagementService } from '../_services/barter-management.service';
import { TransactionFlow } from '../_models/barter-management.model';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-view-balance',
  templateUrl: './view-balance.component.html',
  styleUrls: ['./view-balance.component.css']
})
export class ViewBalanceComponent {
  action: string;
  local_data: any;
  popUpForm: FormGroup;

  transactionFlowList: Array<TransactionFlow> = [] as Array<TransactionFlow>;
  inviteSum = 0;
  ownSum = 0;
  subSum = 0;
  spentSum = 0;
  totalSum = 0;

  constructor
    (
      private router: Router,      
    private barterManagementService: BarterManagementService,
    private dialogRef: MatDialogRef<ViewBalanceComponent>,
  ) {
    this.refreshBalance();
  }

  ngOnInit() {
  }

  refreshBalance() {    
    this.barterManagementService.transactionFlowList$.subscribe(res => {
      this.transactionFlowList = res as [];

      this.inviteSum = this.transactionFlowList
        .filter(x => x.transactionType == 0)
        .reduce((sum, b) => sum + b.amount, 0);

      this.ownSum = this.transactionFlowList
        .filter(x => x.transactionType == 1 || x.transactionType == 4)
        .reduce((sum, b) => sum + b.amount, 0);

      this.subSum = this.transactionFlowList
        .filter(x => x.transactionType == 2)
        .reduce((sum, b) => sum + b.amount, 0);

      this.spentSum = this.transactionFlowList
        .filter(x => x.transactionType == 3)
        .reduce((sum, b) => sum + b.amount, 0);

      this.totalSum = this.inviteSum + this.ownSum + this.subSum + this.spentSum;
      
    }, err => {
     
    }
    );
  }
}
