import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { SpinnerService } from '../_services/spinner.service';
import { AuthenticationService } from '../_services/authentication.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  popUpForm: FormGroup;

  constructor
    (
      private SpinnerService: SpinnerService,
      private dialogRef: MatDialogRef<ContactUsComponent>,
      private formBuilder: FormBuilder,
      private authenticationService: AuthenticationService,
      @Inject('APIUrl') private APIUrl: string
    ) {
  }

  ngOnInit() {
    this.popUpForm = this.formBuilder.group({
      body: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.popUpForm.invalid) {
      alert("A mezők kitöltése kötelező!")
      return;
    }
    else {
      this.SpinnerService.show();
      this.authenticationService.contactUs(this.popUpForm.controls['body'].value).subscribe(res => {
        if (res != undefined) {
          this.SpinnerService.hide();
          alert("Üzenet elküldve.");
          this.dialogRef.close();
        } else {
          this.SpinnerService.hide();
          alert("Ez most nem sikerült.");
        }
        
      },
        error => { this.SpinnerService.hide();});
    }
  }
}
