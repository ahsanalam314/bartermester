import { NgModule, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { AccountManagementRoutingModule } from './account-management-routing.module';
import { EditAccountComponent } from './edit-account/edit-account.component';
import { NgcCookieConsentModule } from 'ngx-cookieconsent';
@NgModule({
  declarations: [EditAccountComponent],
  entryComponents: [EditAccountComponent],
  imports: [
    CommonModule,
    AccountManagementRoutingModule,
    MaterialModule,
    NgcCookieConsentModule
  ]
})
export class AccountManagementModule { }
