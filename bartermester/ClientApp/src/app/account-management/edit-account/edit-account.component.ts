import { Component, OnInit, Optional, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SpinnerService } from '../../_services/spinner.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { User } from '../../_models/user';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.css']
})
export class EditAccountComponent {
  action: string;
  local_data: any;
  popUpForm: FormGroup;

  constructor(
    private SpinnerService: SpinnerService,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditAccountComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: User
  ) {

    this.local_data = { ...data };
    this.action = this.local_data.action;
    this.popUpForm = this.formBuilder.group({
      id: [data.id],
      //name: [data.name, Validators.required],
      //singlePrice: [data.singlePrice, Validators.required],
      //unitNumber: [data.unitNumber, Validators.required],
      //discount: [data.discount, Validators.required],
      //age: [data.age],
      //description: [data.description, Validators.required],
      //locationModelId: [data.locationModelId, Validators.required],
      //conditionModelId: [data.conditionModelId, Validators.required],
      //categoryModelId: [data.categoryModelId, Validators.required],
      //userName: [data.user.username],
      //userId: [data.userId],
      //statusModelGuid: [data.statusModelGuid],
      //postedAt: [data.postedAt],
      //delivery: ['3-5 munkanap']
    });
  }

  onSubmit() {
    if (this.popUpForm.invalid) {
      return;
    }
    //this.SpinnerService.show();
    //this.ItemManagementService.modifyItem(this.popUpForm.value as ItemModel).subscribe(data => {
    //  alert("Mentés kész.");
    //  this.dialogRef.close(data);
    //  this.SpinnerService.hide();
    //});
  }
}
