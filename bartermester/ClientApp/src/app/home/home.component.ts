import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SpinnerService } from '../_services/spinner.service';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {

  constructor(
    private router: Router,
    private SpinnerService: SpinnerService,) { }

  ngOnInit() {
    this.SpinnerService.show();
    this.router.navigate(['/MarketPlace/MarketPlace']);
  }
}
